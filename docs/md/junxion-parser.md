

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public string `[`basename`](#group__parser_1ga54454fd0499b1cd3379760fee5e7aad2)`(const string & pathname,const bool remove_ext)`            | 
`public Json::Value `[`adjacent_line`](#group__parser_1gaa0bd030ea4d25e2cd44b3decebd44e08)`(vector< `[`Point`](#structPoint)` > points)`            | 
`public Json::Value `[`adjacent_geometry`](#group__parser_1ga9a9db073e3fd3e237ce3624fed5d5391)`(const uint64_t uid,const string & side,vector< `[`Point`](#structPoint)` > points)`            | 
`public int `[`main`](#group__parser_1ga3c04138a5bfe5d72780bb7e82a18e627)`(int argc,char ** argv)`            | 
`struct `[`JunxionHandler`](docs/md/junxion-parser.md#structJunxionHandler) | 

## Members

#### `public string `[`basename`](#group__parser_1ga54454fd0499b1cd3379760fee5e7aad2)`(const string & pathname,const bool remove_ext)` 

#### `public Json::Value `[`adjacent_line`](#group__parser_1gaa0bd030ea4d25e2cd44b3decebd44e08)`(vector< `[`Point`](#structPoint)` > points)` 

#### `public Json::Value `[`adjacent_geometry`](#group__parser_1ga9a9db073e3fd3e237ce3624fed5d5391)`(const uint64_t uid,const string & side,vector< `[`Point`](#structPoint)` > points)` 

#### `public int `[`main`](#group__parser_1ga3c04138a5bfe5d72780bb7e82a18e627)`(int argc,char ** argv)` 

# struct `JunxionHandler` 

```
struct JunxionHandler
  : public Handler
```  

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public `[`DataBuffer`](#classDataBuffer)` * `[`data_buffer`](#structJunxionHandler_1a05f0cb171a5aaa15aab4e1e1a29319c0) | 
`public const unsigned short `[`adjacent_offset`](docs/md/junxion-parser.md#structJunxionHandler_1add213d1087938544605c1c2530844600) | 
`public uint64_t `[`nodes`](docs/md/junxion-parser.md#structJunxionHandler_1a8c7d6dd490cc6db444969c1eece09890) | 
`public uint64_t `[`ways`](docs/md/junxion-parser.md#structJunxionHandler_1af3a8cfb6f6831363b1db03afb38afaf2) | 
`public inline  `[`JunxionHandler`](#structJunxionHandler_1aba596f0e050b75ccd9e7b1a0c9572871)`(unsigned short offset)` | Callback handler `struct` that's responsible for collecting junxions on top of `osmium`.
`public inline void `[`way`](#structJunxionHandler_1aa0bdfba8fdc318128e180d89c6662c3e)`(const Way & way) noexcept` | Retrieve locations of all nodes in the way from storage and add them to the way object.
`public inline const `[`Point`](#structPoint)` `[`adjacent_point`](#structJunxionHandler_1a4a4e95dee7ed3bba6efda98a61b30b59)`(const NodeRef & node)` | 
`public inline void `[`add_adjacent`](#structJunxionHandler_1aac5af4e7406fd1ce2cf198939fa787f5)`(uint64_t uid,const Way & way,unsigned short idx,const unsigned short offset)` | 

## Members

#### `public `[`DataBuffer`](#classDataBuffer)` * `[`data_buffer`](#structJunxionHandler_1a05f0cb171a5aaa15aab4e1e1a29319c0) 

#### `public const unsigned short `[`adjacent_offset`](docs/md/junxion-parser.md#structJunxionHandler_1add213d1087938544605c1c2530844600) 

#### `public uint64_t `[`nodes`](docs/md/junxion-parser.md#structJunxionHandler_1a8c7d6dd490cc6db444969c1eece09890) 

#### `public uint64_t `[`ways`](docs/md/junxion-parser.md#structJunxionHandler_1af3a8cfb6f6831363b1db03afb38afaf2) 

#### `public inline  `[`JunxionHandler`](#structJunxionHandler_1aba596f0e050b75ccd9e7b1a0c9572871)`(unsigned short offset)` 

Callback handler `struct` that's responsible for collecting junxions on top of `osmium`.

the amount of ajdacent nodes to join on each side of the way a junxion is on.

#### `public inline void `[`way`](#structJunxionHandler_1aa0bdfba8fdc318128e180d89c6662c3e)`(const Way & way) noexcept` 

Retrieve locations of all nodes in the way from storage and add them to the way object.

#### `public inline const `[`Point`](#structPoint)` `[`adjacent_point`](#structJunxionHandler_1a4a4e95dee7ed3bba6efda98a61b30b59)`(const NodeRef & node)` 

#### `public inline void `[`add_adjacent`](#structJunxionHandler_1aac5af4e7406fd1ce2cf198939fa787f5)`(uint64_t uid,const Way & way,unsigned short idx,const unsigned short offset)` 

