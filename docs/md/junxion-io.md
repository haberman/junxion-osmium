

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`class `[`Junxion`](docs/md/junxion-io.md#classJunxion) | Describes a junxion, which is sort of an augmented regular OSM `node`.
`class `[`DataBuffer`](docs/md/junxion-io.md#classDataBuffer) | Singleton class that exposes data needed a little bit everywhere.
`class `[`InputParser`](docs/md/junxion-io.md#classInputParser) | 
`struct `[`Point`](docs/md/junxion-io.md#structPoint) | Holds an OSM node as a pair of coordinates: a geographical location, and a -1/1 normalized screen projection.
`struct `[`Adjacent`](docs/md/junxion-io.md#structAdjacent) | Holds two lists containing nodes being either before or after a junxion.

# class `Junxion` 

Describes a junxion, which is sort of an augmented regular OSM `node`.

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public unsigned short `[`connections`](docs/md/junxion-io.md#classJunxion_1a50253923f20d0ea2ea8d933dc43798d1) | 
`public float `[`weight`](docs/md/junxion-io.md#classJunxion_1adc91303945cb212e1fe889d52a5a8bdf) | 
`public `[`Point`](#structPoint)` `[`point`](#classJunxion_1acd6cc0eb1ad97a7f762105d88ad5bfae) | 
`public map< uint64_t, `[`Adjacent`](#structAdjacent)` > `[`adjacents`](#classJunxion_1a488400352772a40c5fec8b3894f2f918) | 
`public inline  `[`Junxion`](#classJunxion_1a8fe7bca91139f78b9052b8573a92583e)`()` | Empty constructor.
`public  `[`Junxion`](#classJunxion_1a485c6127766064c33b5986d53c1b2552)`(const uint64_t uid)` | Constructs a new `[Junxion](docs/md/junxion-io.md#classJunxion)` with an initial node id.
`public  `[`Junxion`](#classJunxion_1aab0d141c31a076e8a80faa3d25bce0f0)`(const uint64_t uid,const Location location)` | Constructs a new `[Junxion](docs/md/junxion-io.md#classJunxion)` with a complete OSM node.
`public void `[`print`](#classJunxion_1a19ac9b4119a820323a1bd65f13856784)`()` | Logs important informations about the `[Junxion](docs/md/junxion-io.md#classJunxion)` content and state.
`public `[`Junxion`](#classJunxion)` & `[`operator=`](#classJunxion_1a4d4420a26034f3d4bfe8dc48f7007722)`(const `[`Junxion`](#classJunxion)` &)` | 

## Members

#### `public unsigned short `[`connections`](docs/md/junxion-io.md#classJunxion_1a50253923f20d0ea2ea8d933dc43798d1) 

#### `public float `[`weight`](docs/md/junxion-io.md#classJunxion_1adc91303945cb212e1fe889d52a5a8bdf) 

#### `public `[`Point`](#structPoint)` `[`point`](#classJunxion_1acd6cc0eb1ad97a7f762105d88ad5bfae) 

#### `public map< uint64_t, `[`Adjacent`](#structAdjacent)` > `[`adjacents`](#classJunxion_1a488400352772a40c5fec8b3894f2f918) 

#### `public inline  `[`Junxion`](#classJunxion_1a8fe7bca91139f78b9052b8573a92583e)`()` 

Empty constructor.

#### `public  `[`Junxion`](#classJunxion_1a485c6127766064c33b5986d53c1b2552)`(const uint64_t uid)` 

Constructs a new `[Junxion](#classJunxion)` with an initial node id.

#### Parameters
* `uid` OSM node id

#### `public  `[`Junxion`](#classJunxion_1aab0d141c31a076e8a80faa3d25bce0f0)`(const uint64_t uid,const Location location)` 

Constructs a new `[Junxion](#classJunxion)` with a complete OSM node.

#### Parameters
* `uid` OSM node id 

#### Returns
osmium `Location`

#### `public void `[`print`](#classJunxion_1a19ac9b4119a820323a1bd65f13856784)`()` 

Logs important informations about the `[Junxion](#classJunxion)` content and state.

#### `public `[`Junxion`](#classJunxion)` & `[`operator=`](#classJunxion_1a4d4420a26034f3d4bfe8dc48f7007722)`(const `[`Junxion`](#classJunxion)` &)` 

# class `DataBuffer` 

Singleton class that exposes data needed a little bit everywhere.

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public const Vector2f * `[`pixel`](docs/md/junxion-io.md#classDataBuffer_1aee2d68745bf1152e2bcbbcb7057cca66) | Size of a pixel expressed as a fraction of (-1/1) screen space.
`public void `[`destroy_instance`](#classDataBuffer_1af7e0b95849c5eba98e58118f2d84ad23)`()` | 

## Members

#### `public const Vector2f * `[`pixel`](docs/md/junxion-io.md#classDataBuffer_1aee2d68745bf1152e2bcbbcb7057cca66) 

Size of a pixel expressed as a fraction of (-1/1) screen space.

#### `public void `[`destroy_instance`](#classDataBuffer_1af7e0b95849c5eba98e58118f2d84ad23)`()` 

# class `InputParser` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public  `[`InputParser`](#classInputParser_1af9fa5ead1f28b5294a713410df5b9531)`(int & argc,char ** argv)` | 
`public const string & `[`get_argument`](#classInputParser_1a8e31209ceee7abd20b7bc96c8bddcf56)`() const` | Returns last token.
`public const string & `[`get_argument_s`](#classInputParser_1a3b284dc74f951f2175128cac6011f419)`(const string & option,const string & s) const` | 
`public const bool `[`get_argument`](#classInputParser_1ae393f5269bb284ce706ba83cd329d54c)`(const string & option,const bool b) const` | 
`public const unsigned int `[`get_argument`](#classInputParser_1acc0418363da14f91f370af19dc8e80ac)`(const string & option,const int i) const` | 
`public const bool `[`argument_exists`](#classInputParser_1ac2fa9f8d060cd556a53a033cd2d6f2d4)`(const string & option) const` | 

## Members

#### `public  `[`InputParser`](#classInputParser_1af9fa5ead1f28b5294a713410df5b9531)`(int & argc,char ** argv)` 

#### `public const string & `[`get_argument`](#classInputParser_1a8e31209ceee7abd20b7bc96c8bddcf56)`() const` 

Returns last token.

#### `public const string & `[`get_argument_s`](#classInputParser_1a3b284dc74f951f2175128cac6011f419)`(const string & option,const string & s) const` 

#### `public const bool `[`get_argument`](#classInputParser_1ae393f5269bb284ce706ba83cd329d54c)`(const string & option,const bool b) const` 

#### `public const unsigned int `[`get_argument`](#classInputParser_1acc0418363da14f91f370af19dc8e80ac)`(const string & option,const int i) const` 

#### `public const bool `[`argument_exists`](#classInputParser_1ac2fa9f8d060cd556a53a033cd2d6f2d4)`(const string & option) const` 

# struct `Point` 

Holds an OSM node as a pair of coordinates: a geographical location, and a -1/1 normalized screen projection.

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public uint64_t `[`uid`](docs/md/junxion-io.md#structPoint_1ade498a73d38c81f2feed5852eb6bc262) | 
`public Location `[`location`](docs/md/junxion-io.md#structPoint_1ab0c397da2db30a294b1105379b6e44ca) | 
`public Vector2f `[`projection`](docs/md/junxion-io.md#structPoint_1aad52ba789cb59098c81f3f7bcf095cd3) | 

## Members

#### `public uint64_t `[`uid`](docs/md/junxion-io.md#structPoint_1ade498a73d38c81f2feed5852eb6bc262) 

#### `public Location `[`location`](docs/md/junxion-io.md#structPoint_1ab0c397da2db30a294b1105379b6e44ca) 

#### `public Vector2f `[`projection`](docs/md/junxion-io.md#structPoint_1aad52ba789cb59098c81f3f7bcf095cd3) 

# struct `Adjacent` 

Holds two lists containing nodes being either before or after a junxion.

the way we identify an adjacent using a `way_id` is wrong since we could have multiple ajdacents on a single way.

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public uint64_t `[`way_uid`](docs/md/junxion-io.md#structAdjacent_1a07c797c4caa6c69cbca6cac9c8b8e9c2) | 
`public vector< `[`Point`](#structPoint)` > `[`after`](#structAdjacent_1ae423730dc35bdbfbaaf2ccf26e6897d7) | 
`public vector< `[`Point`](#structPoint)` > `[`before`](#structAdjacent_1ae773c57900ea3ca722b3274b25d75940) | 

## Members

#### `public uint64_t `[`way_uid`](docs/md/junxion-io.md#structAdjacent_1a07c797c4caa6c69cbca6cac9c8b8e9c2) 

#### `public vector< `[`Point`](#structPoint)` > `[`after`](#structAdjacent_1ae423730dc35bdbfbaaf2ccf26e6897d7) 

#### `public vector< `[`Point`](#structPoint)` > `[`before`](#structAdjacent_1ae773c57900ea3ca722b3274b25d75940) 

