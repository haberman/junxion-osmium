

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`class `[`KdTree`](docs/md/junxion-cgal.md#classKdTree) | This class wraps a `CGAL`[Kd_tree](https://doc.cgal.org/latest/Spatial_searching/classCGAL_1_1Kd__tree.html) and exposes methods that facilitates traversing.
`class `[`Triangulator`](docs/md/junxion-cgal.md#classTriangulator) | This class pipes input points to different `CGAL`[triangulation](https://doc.cgal.org/latest/Triangulation_2/index.html#Chapter_2D_Triangulations)'s components.
`class `[`Voronoi`](docs/md/junxion-cgal.md#classVoronoi) | This class pipes input points to a `CGAL`[Voronoi](https://doc.cgal.org/latest/Voronoi_diagram_2/index.html#Chapter_2D_Voronoi_Diagram_Adaptor) diagram.

# class `KdTree` 

This class wraps a `CGAL`[Kd_tree](https://doc.cgal.org/latest/Spatial_searching/classCGAL_1_1Kd__tree.html) and exposes methods that facilitates traversing.

TODO -> well, draw the shit!

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public inline  `[`KdTree`](#classKdTree_1a396f88555233f00bc9076b7ad0f6e90a)`(shared_ptr< `[`ColorGradient`](#classColorGradient)` > fill_gradient)` | Constructor.
`public  `[`~KdTree`](#classKdTree_1ae9440a092ce2c216714e84d32bf041d2)`()` | Destructor.
`public void `[`insert`](#classKdTree_1ab6c5203f5d61d845b6fd04a98232632a)`(const sf::Vector2f &)` | Inserts a new point.
`public void `[`draw`](#classKdTree_1a3cf3c3a0550fe125a27a605d81e6140b)`(const bool fill)` | Draws the KD diagram.
`public void `[`print`](#classKdTree_1a86030da978775b8ff149b80f065c5d8a)`()` | Prints the tree's data in the terminal.

## Members

#### `public inline  `[`KdTree`](#classKdTree_1a396f88555233f00bc9076b7ad0f6e90a)`(shared_ptr< `[`ColorGradient`](#classColorGradient)` > fill_gradient)` 

Constructor.

#### Parameters
* `fill_gradient` A [ColorGradient](#classColorGradient) instance used to map a ramp of colors to a 3rd value input

#### `public  `[`~KdTree`](#classKdTree_1ae9440a092ce2c216714e84d32bf041d2)`()` 

Destructor.

#### `public void `[`insert`](#classKdTree_1ab6c5203f5d61d845b6fd04a98232632a)`(const sf::Vector2f &)` 

Inserts a new point.

#### Parameters
* `point` A `sf::Vectorf2f` coordinate

#### `public void `[`draw`](#classKdTree_1a3cf3c3a0550fe125a27a605d81e6140b)`(const bool fill)` 

Draws the KD diagram.

#### Parameters
* `fill` Whether to draw as outlines or filled areas

#### `public void `[`print`](#classKdTree_1a86030da978775b8ff149b80f065c5d8a)`()` 

Prints the tree's data in the terminal.

# class `Triangulator` 

This class pipes input points to different `CGAL`[triangulation](https://doc.cgal.org/latest/Triangulation_2/index.html#Chapter_2D_Triangulations)'s components.

TODO -> Make a composition out of 2 triangulation's methods: a (more expressive) Delaunay and a constrained one.

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public inline  `[`Triangulator`](#classTriangulator_1a92fafde01ed77cd4b4a0fc31b4e24a3d)`(shared_ptr< `[`ColorGradient`](#classColorGradient)` > fill_gradient)` | Constructor.
`public  `[`~Triangulator`](#classTriangulator_1adc583a706e1d2c863c5bfbeb713e7592)`()` | Destructor.
`public void `[`insert`](#classTriangulator_1aee673a055f551d84a56d5c46985709e3)`(const Vector2f & point)` | Insert a new point.
`public void `[`draw`](#classTriangulator_1a9c855ff3a39e923e6a2d005febd0306e)`(const bool fill)` | Draws the Delaunay triangulation.

## Members

#### `public inline  `[`Triangulator`](#classTriangulator_1a92fafde01ed77cd4b4a0fc31b4e24a3d)`(shared_ptr< `[`ColorGradient`](#classColorGradient)` > fill_gradient)` 

Constructor.

#### Parameters
* `fill_gradient` A `[ColorGradient](#classColorGradient)` instance used to map a ramp of colors to a 3rd value input

#### `public  `[`~Triangulator`](#classTriangulator_1adc583a706e1d2c863c5bfbeb713e7592)`()` 

Destructor.

#### `public void `[`insert`](#classTriangulator_1aee673a055f551d84a56d5c46985709e3)`(const Vector2f & point)` 

Insert a new point.

#### Parameters
* `point` A `sf::Vectorf2f` coordinate

#### `public void `[`draw`](#classTriangulator_1a9c855ff3a39e923e6a2d005febd0306e)`(const bool fill)` 

Draws the Delaunay triangulation.

#### Parameters
* `fill` Whether to draw the triangulation as outlines or filled areas

# class `Voronoi` 

This class pipes input points to a `CGAL`[Voronoi](https://doc.cgal.org/latest/Voronoi_diagram_2/index.html#Chapter_2D_Voronoi_Diagram_Adaptor) diagram.

TODO -> Make 2 voronoi diagrams by adding an [Apollonius](https://doc.cgal.org/latest/Apollonius_graph_2/index.html#Chapter_2D_Apollonius_Graphs) graph.

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public inline  `[`Voronoi`](#classVoronoi_1a27031e18cdf94c3c0c1550340c469728)`(shared_ptr< `[`ColorGradient`](#classColorGradient)` > fill_gradient)` | Constructor.
`public  `[`~Voronoi`](#classVoronoi_1a9f4428492a0608030343ffbd4bb8d9ae)`()` | Destructor.
`public void `[`insert`](#classVoronoi_1afdf7254ac34f04cf604908da678ea7d5)`(const sf::Vector2f & point)` | Inserts a new point.
`public void `[`draw`](#classVoronoi_1ac0c0e9dc63100e62ceef66775181c260)`(const bool fill)` | Draws the [Voronoi](docs/md/junxion-cgal.md#classVoronoi) diagram.

## Members

#### `public inline  `[`Voronoi`](#classVoronoi_1a27031e18cdf94c3c0c1550340c469728)`(shared_ptr< `[`ColorGradient`](#classColorGradient)` > fill_gradient)` 

Constructor.

#### Parameters
* `fill_gradient` A [ColorGradient](#classColorGradient) instance used to map a ramp of colors to a 3rd value input

#### `public  `[`~Voronoi`](#classVoronoi_1a9f4428492a0608030343ffbd4bb8d9ae)`()` 

Destructor.

#### `public void `[`insert`](#classVoronoi_1afdf7254ac34f04cf604908da678ea7d5)`(const sf::Vector2f & point)` 

Inserts a new point.

#### Parameters
* `point` A `sf::Vectorf2f` coordinate

#### `public void `[`draw`](#classVoronoi_1ac0c0e9dc63100e62ceef66775181c260)`(const bool fill)` 

Draws the [Voronoi](#classVoronoi) diagram.

#### Parameters
* `fill` Whether to draw as outlines or filled areas

