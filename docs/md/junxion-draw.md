

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`enum `[`GradientStyle`](#group__draw_1gaa6b9d577a1700f29923f49f7b77d165f)            | Some gradient presets picked over [here](https://uigradients.com/).
`enum `[`GradientType`](#group__draw_1ga0bb3c96a5ae4411bf72bdafaaaaec12a)            | How gradients are meant to be interpolated over geometry.
`public  `[`ColorGradient`](#group__draw_1gaf28d6938521e33c037d8b309b7c2a1ec)`(const `[`GradientStyle`](#group__draw_1gaa6b9d577a1700f29923f49f7b77d165f)` & style)`            | Default constructor could use any hard-coded value of the `GradientStyle` enum.
`public  `[`ColorGradient`](#group__draw_1ga8ba4f0c70db58dac9dcfc3c4f5a8d7a5)`(const vector< sf::Color > & colors)`            | Constructor that accepts a user provided list of colors.
`public inline  `[`~ColorGradient`](#group__draw_1ga12b62b0bd7abf5292d8a936e753657c9)`()`            | Destructor.
`public void `[`add_color_point`](#group__draw_1ga1eed307395cbdc96dd569baff8fcbdd9)`(const Color & color,const float position)`            | Inserts a new color point into its correct position.
`public void `[`clear_gradient`](#group__draw_1gad693b08cfad8ad56752e7e0e4a7278aa)`()`            | Clears the gradient colors vector.
`public void `[`create`](#group__draw_1ga3f2e7d9071bc399cd3aa392cf18f01c8)`(const `[`GradientStyle`](#group__draw_1gaa6b9d577a1700f29923f49f7b77d165f)` & gradient_type)`            | Places a default color heapmap gradient into the `color` vector.
`public const Color `[`color_at`](#group__draw_1ga82f2731ba5386e4ad430d23cb6822291)`(const float position) const`            | Returns the color at the given position.
`public const vector< GLfloat > `[`gl_color_at`](#group__draw_1ga7bbb6c9431f4dcba4d172ff9e201d4ce)`(const float position) const`            | Returns the color at the given position.
`public const size_t `[`size`](#group__draw_1ga2a25f023ee329f2055e228e75b918748)`() const`            | Returns the amount of colors within the gradient ramp.
`public inline  `[`ColorPoint`](#group__draw_1gaf0c1330bab64cbb6b6515aa2aabf5b3f)`(Color _color,float _position)`            | Constructor.
`public static void `[`draw_junxion`](#group__draw_1ga28c4f28e3bebf9c22294c61955dce694)`(const `[`Junxion`](#classJunxion)` & junxion,const Vector2f & radius,`[`GLStyle`](#structGLStyle)` & style)`            | Draws a junxion which consists in:
`public static void `[`draw_circle`](#group__draw_1ga32078e6fec2ef62eccdae5a2cc495c6a)`(const Vector2f & center,const Vector2f & radius,const unsigned int steps,GLenum gl_mode)`            | Draws a circle.
`public static void `[`draw_rectangle`](#group__draw_1gac759ad99e5f8e73f2f0d0b56bb7d2c3f)`(const Vector2f & top_left,const Vector2f & bottom_right,GLenum gl_mode)`            | Draws a rectangle.
`public static void `[`draw_cross`](#group__draw_1ga353c9805680e03a95a3ca3a12ea637b8)`(const Vector2f & center,const Vector2f & radius)`            | Draws a cross.
`public static void `[`draw_triangles`](#group__draw_1gab7d581b432cb8f4be272ff3fc9a96d32)`(const vector< Vector2f > & vertices,GLenum gl_mode)`            | Draws a triangle.
`private inline static void `[`set_color`](#group__draw_1ga532d4ab0ceacac92e9585908ecc194db)`(const Color & c)`            | Shortcut to set the `glColor4f()` out of a (255ly scaled) `sf:Color` reference.
`class `[`ColorGradient`](docs/md/junxion-draw.md#classColorGradient) | This class offers methods to interact with gradients as made by:
`class `[`GLDrawer`](docs/md/junxion-draw.md#classGLDrawer) | This all-static class offers shortcuts to draw more or less complex things.
`struct `[`ColorGradient::ColorPoint`](docs/md/junxion-draw.md#structColorGradient_1_1ColorPoint) | Internal `struct` used to store a color stop inside the [ColorGradient](docs/md/junxion-draw.md#classColorGradient).
`struct `[`GLStyle`](docs/md/junxion-draw.md#structGLStyle) | Small `struct` holding the main attributes of any drawing calls.

## Members

#### `enum `[`GradientStyle`](#group__draw_1gaa6b9d577a1700f29923f49f7b77d165f) 

 Values                         | Descriptions                                
--------------------------------|---------------------------------------------
DigitalWater            | Pastel blue.
Mango            | Light sunrise yellow.
RedSunset            | Late sunset blue.
RelaxingRed            | Bright yellow to pomegranate red.
SandBlue            | Desaturated dark blue to yellow.
Shifter            | Purple to pink.
Windy            | Bright purple to icy blue.
WeddingDayBlues            | Cyan-orange-pink.

Some gradient presets picked over [here](https://uigradients.com/).

#### `enum `[`GradientType`](#group__draw_1ga0bb3c96a5ae4411bf72bdafaaaaec12a) 

 Values                         | Descriptions                                
--------------------------------|---------------------------------------------
PER_LINE            | Over the entire line.
PER_WEIGHT            | Over end point as their normalized min/max weight express the connections amount.
PER_AMOUNT            | Over the amount of junxions inside the partition of a quad tree.

How gradients are meant to be interpolated over geometry.

#### `public  `[`ColorGradient`](#group__draw_1gaf28d6938521e33c037d8b309b7c2a1ec)`(const `[`GradientStyle`](#group__draw_1gaa6b9d577a1700f29923f49f7b77d165f)` & style)` 

Default constructor could use any hard-coded value of the `GradientStyle` enum.

#### Parameters
* `style` One of the `GradientStyle` member

#### `public  `[`ColorGradient`](#group__draw_1ga8ba4f0c70db58dac9dcfc3c4f5a8d7a5)`(const vector< sf::Color > & colors)` 

Constructor that accepts a user provided list of colors.

#### Parameters
* `colors` Reference to a `vector` of `sf::Color`

#### `public inline  `[`~ColorGradient`](#group__draw_1ga12b62b0bd7abf5292d8a936e753657c9)`()` 

Destructor.

#### `public void `[`add_color_point`](#group__draw_1ga1eed307395cbdc96dd569baff8fcbdd9)`(const Color & color,const float position)` 

Inserts a new color point into its correct position.

#### Parameters
* `color` The `sf::Color` to insert 

* `position` Where to insert the color in the ramp

#### `public void `[`clear_gradient`](#group__draw_1gad693b08cfad8ad56752e7e0e4a7278aa)`()` 

Clears the gradient colors vector.

#### `public void `[`create`](#group__draw_1ga3f2e7d9071bc399cd3aa392cf18f01c8)`(const `[`GradientStyle`](#group__draw_1gaa6b9d577a1700f29923f49f7b77d165f)` & gradient_type)` 

Places a default color heapmap gradient into the `color` vector.

#### `public const Color `[`color_at`](#group__draw_1ga82f2731ba5386e4ad430d23cb6822291)`(const float position) const` 

Returns the color at the given position.

#### Parameters
* `position` Where the color should be computed. 

#### Returns
`sf::Color`.

#### `public const vector< GLfloat > `[`gl_color_at`](#group__draw_1ga7bbb6c9431f4dcba4d172ff9e201d4ce)`(const float position) const` 

Returns the color at the given position.

#### Parameters
* `position` Where the color should be computed. 

#### Returns
`vector` of RGBA `GLfloat`.

#### `public const size_t `[`size`](#group__draw_1ga2a25f023ee329f2055e228e75b918748)`() const` 

Returns the amount of colors within the gradient ramp.

#### `public inline  `[`ColorPoint`](#group__draw_1gaf0c1330bab64cbb6b6515aa2aabf5b3f)`(Color _color,float _position)` 

Constructor.

#### Parameters
* `color_` `sf::Color` of this stop 

* `position_` Position of this stop in the gradient

#### `public static void `[`draw_junxion`](#group__draw_1ga28c4f28e3bebf9c22294c61955dce694)`(const `[`Junxion`](#classJunxion)` & junxion,const Vector2f & radius,`[`GLStyle`](#structGLStyle)` & style)` 

Draws a junxion which consists in:

* a circle of different color and size;

* a text fragment simply reporting the junxion node's id.

#### Parameters
* `junxion` Reference to the `[Junxion](#classJunxion)` to draw 

* `radius` The 2D standard radius of the circle (might be affected by junxion weight during drawing) 

* `style` Reference to a `[GLStyle](#structGLStyle)` struct

#### `public static void `[`draw_circle`](#group__draw_1ga32078e6fec2ef62eccdae5a2cc495c6a)`(const Vector2f & center,const Vector2f & radius,const unsigned int steps,GLenum gl_mode)` 

Draws a circle.

#### Parameters
* `position` The position the circle 

* `radius` The 2D standard radius of the circle (might be affected by junxion weight during drawing) 

* `steps` Amount of iterations (sides) used to generate the circle (default: `32`) 

* `gl_mode` `GLenum` that tells how vertices should be linked together (default: `GL_TRIANGLE_FAN`)

#### `public static void `[`draw_rectangle`](#group__draw_1gac759ad99e5f8e73f2f0d0b56bb7d2c3f)`(const Vector2f & top_left,const Vector2f & bottom_right,GLenum gl_mode)` 

Draws a rectangle.

#### Parameters
* `top_left` The top left position of the rectangle 

* `bottom_right` The bottom right position of the rectangle 

* `gl_mode` `GLenum` that tells how vertices should be linked together (default: `GL_LINE_LOOP`)

#### `public static void `[`draw_cross`](#group__draw_1ga353c9805680e03a95a3ca3a12ea637b8)`(const Vector2f & center,const Vector2f & radius)` 

Draws a cross.

#### Parameters
* `center` The position of the cross 

* `radius` The 2D standard radius of the cross (might be affected by junxion weight during drawing)

#### `public static void `[`draw_triangles`](#group__draw_1gab7d581b432cb8f4be272ff3fc9a96d32)`(const vector< Vector2f > & vertices,GLenum gl_mode)` 

Draws a triangle.

#### Parameters
* `vertices` A flat list of points where each successive triplets describes a triangle face. 

* `gl_mode` `GLenum` that tells how vertices should be linked together (default: `GL_TRIANGLES`)

#### `private inline static void `[`set_color`](#group__draw_1ga532d4ab0ceacac92e9585908ecc194db)`(const Color & c)` 

Shortcut to set the `glColor4f()` out of a (255ly scaled) `sf:Color` reference.

#### Parameters
* `c` The input color

# class `ColorGradient` 

This class offers methods to interact with gradients as made by:

* a list of colors;

* an interpolation function that translates a one-dimensional position to RGB.

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public  `[`ColorGradient`](#group__draw_1gaf28d6938521e33c037d8b309b7c2a1ec)`(const `[`GradientStyle`](#group__draw_1gaa6b9d577a1700f29923f49f7b77d165f)` & style)` | Default constructor could use any hard-coded value of the `GradientStyle` enum.
`public  `[`ColorGradient`](#group__draw_1ga8ba4f0c70db58dac9dcfc3c4f5a8d7a5)`(const vector< sf::Color > & colors)` | Constructor that accepts a user provided list of colors.
`public inline  `[`~ColorGradient`](#group__draw_1ga12b62b0bd7abf5292d8a936e753657c9)`()` | Destructor.
`public void `[`add_color_point`](#group__draw_1ga1eed307395cbdc96dd569baff8fcbdd9)`(const Color & color,const float position)` | Inserts a new color point into its correct position.
`public void `[`clear_gradient`](#group__draw_1gad693b08cfad8ad56752e7e0e4a7278aa)`()` | Clears the gradient colors vector.
`public void `[`create`](#group__draw_1ga3f2e7d9071bc399cd3aa392cf18f01c8)`(const `[`GradientStyle`](#group__draw_1gaa6b9d577a1700f29923f49f7b77d165f)` & gradient_type)` | Places a default color heapmap gradient into the `color` vector.
`public const Color `[`color_at`](#group__draw_1ga82f2731ba5386e4ad430d23cb6822291)`(const float position) const` | Returns the color at the given position.
`public const vector< GLfloat > `[`gl_color_at`](#group__draw_1ga7bbb6c9431f4dcba4d172ff9e201d4ce)`(const float position) const` | Returns the color at the given position.
`public const size_t `[`size`](#group__draw_1ga2a25f023ee329f2055e228e75b918748)`() const` | Returns the amount of colors within the gradient ramp.

## Members

#### `public  `[`ColorGradient`](#group__draw_1gaf28d6938521e33c037d8b309b7c2a1ec)`(const `[`GradientStyle`](#group__draw_1gaa6b9d577a1700f29923f49f7b77d165f)` & style)` 

Default constructor could use any hard-coded value of the `GradientStyle` enum.

#### Parameters
* `style` One of the `GradientStyle` member

#### `public  `[`ColorGradient`](#group__draw_1ga8ba4f0c70db58dac9dcfc3c4f5a8d7a5)`(const vector< sf::Color > & colors)` 

Constructor that accepts a user provided list of colors.

#### Parameters
* `colors` Reference to a `vector` of `sf::Color`

#### `public inline  `[`~ColorGradient`](#group__draw_1ga12b62b0bd7abf5292d8a936e753657c9)`()` 

Destructor.

#### `public void `[`add_color_point`](#group__draw_1ga1eed307395cbdc96dd569baff8fcbdd9)`(const Color & color,const float position)` 

Inserts a new color point into its correct position.

#### Parameters
* `color` The `sf::Color` to insert 

* `position` Where to insert the color in the ramp

#### `public void `[`clear_gradient`](#group__draw_1gad693b08cfad8ad56752e7e0e4a7278aa)`()` 

Clears the gradient colors vector.

#### `public void `[`create`](#group__draw_1ga3f2e7d9071bc399cd3aa392cf18f01c8)`(const `[`GradientStyle`](#group__draw_1gaa6b9d577a1700f29923f49f7b77d165f)` & gradient_type)` 

Places a default color heapmap gradient into the `color` vector.

#### `public const Color `[`color_at`](#group__draw_1ga82f2731ba5386e4ad430d23cb6822291)`(const float position) const` 

Returns the color at the given position.

#### Parameters
* `position` Where the color should be computed. 

#### Returns
`sf::Color`.

#### `public const vector< GLfloat > `[`gl_color_at`](#group__draw_1ga7bbb6c9431f4dcba4d172ff9e201d4ce)`(const float position) const` 

Returns the color at the given position.

#### Parameters
* `position` Where the color should be computed. 

#### Returns
`vector` of RGBA `GLfloat`.

#### `public const size_t `[`size`](#group__draw_1ga2a25f023ee329f2055e228e75b918748)`() const` 

Returns the amount of colors within the gradient ramp.

# class `GLDrawer` 

This all-static class offers shortcuts to draw more or less complex things.

All methods expects `GL` normalized (-1/1) coordinates in the form of a `sf::Vector2f`.

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------

## Members

# struct `ColorGradient::ColorPoint` 

Internal `struct` used to store a color stop inside the [ColorGradient](#classColorGradient).

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public Color `[`color`](#group__draw_1ga978a1a06cab7e47831178026285b17a7) | The color.
`public float `[`position`](#group__draw_1ga0432a5213272eed539b0f01414786299) | Position of the color along the gradient (between 0 and 1).
`public inline  `[`ColorPoint`](#group__draw_1gaf0c1330bab64cbb6b6515aa2aabf5b3f)`(Color _color,float _position)` | Constructor.

## Members

#### `public Color `[`color`](#group__draw_1ga978a1a06cab7e47831178026285b17a7) 

The color.

#### `public float `[`position`](#group__draw_1ga0432a5213272eed539b0f01414786299) 

Position of the color along the gradient (between 0 and 1).

#### `public inline  `[`ColorPoint`](#group__draw_1gaf0c1330bab64cbb6b6515aa2aabf5b3f)`(Color _color,float _position)` 

Constructor.

#### Parameters
* `color_` `sf::Color` of this stop 

* `position_` Position of this stop in the gradient

# struct `GLStyle` 

Small `struct` holding the main attributes of any drawing calls.

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public Color `[`fill_color`](#group__draw_1ga870851cc5e4e85deca71e6b3fd80d088) | Fill color (default: black).
`public Color `[`outline_color`](#group__draw_1gadd13b7281efb89b426c8dd6bf6c0b96d) | Outline color (dafault: black).
`public GLfloat `[`outline_width`](#group__draw_1gac3a25da86aacd6e1e1b56298877cc3b6) | Outline width (default: 1).

## Members

#### `public Color `[`fill_color`](#group__draw_1ga870851cc5e4e85deca71e6b3fd80d088) 

Fill color (default: black).

#### `public Color `[`outline_color`](#group__draw_1gadd13b7281efb89b426c8dd6bf6c0b96d) 

Outline color (dafault: black).

#### `public GLfloat `[`outline_width`](#group__draw_1gac3a25da86aacd6e1e1b56298877cc3b6) 

Outline width (default: 1).

