

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public unsigned int `[`output_width_`](#group__drawer_1ga063eda0667ca2100ac907091df8eb330)`(const unsigned int width,const string & mode)`            | A simple method that caps a width up to the screen resolution when mode is `screen`.
`public string `[`basename`](#group__drawer_1gabe32eca6ac8ec9239ef8d2c98cd74c64)`(const string & path,const bool ext)`            | Extracts a file's basename from a given path.
`public void `[`parse_junxions`](#group__drawer_1ga041d679d197d08aadbf89b78500a8909)`(const string geojson_file,Box * bounding_box,float & min_connections,float & max_connections)`            | Fills the [DataBuffer](docs/md/junxion-io.md#classDataBuffer) with junxions coming from a given GeoJSON file.
`public int `[`main`](#group__drawer_1ga3c04138a5bfe5d72780bb7e82a18e627)`(int argc,char ** argv)`            | Main program entry point.

## Members

#### `public unsigned int `[`output_width_`](#group__drawer_1ga063eda0667ca2100ac907091df8eb330)`(const unsigned int width,const string & mode)` 

A simple method that caps a width up to the screen resolution when mode is `screen`.

#### Parameters
* `width` The width to check 

* `mode` The output mode (facilitates the single use we have for this function inside the CLI `main` callback) 

#### Returns
The clamped width.

#### `public string `[`basename`](#group__drawer_1gabe32eca6ac8ec9239ef8d2c98cd74c64)`(const string & path,const bool ext)` 

Extracts a file's basename from a given path.

#### Parameters
* `path` The input path to inspect 

* `ext` Whether we should include the file extension as well 

#### Returns
The file's basename.

#### `public void `[`parse_junxions`](#group__drawer_1ga041d679d197d08aadbf89b78500a8909)`(const string geojson_file,Box * bounding_box,float & min_connections,float & max_connections)` 

Fills the [DataBuffer](#classDataBuffer) with junxions coming from a given GeoJSON file.

The rule is the following: junxion comes as a `GeometryCollection` and contains;

* a single `[Point](#structPoint)` which represents the junxion's node location;

* one or more adjacents as a `MultilineString` list of locations.

#### Parameters
* `geojson_file` The input file 

* `bouding_box` Pointer to a `sf::Box` which will be extended to scale the input data 

* `min_connections` Reference to track the minimum of junxions' connections. 

* `max_connections` Reference to track the maximum of junxions' connections.

#### `public int `[`main`](#group__drawer_1ga3c04138a5bfe5d72780bb7e82a18e627)`(int argc,char ** argv)` 

Main program entry point.

This is a single threaded logic flow from input parsing to output drawing, OpenGL context or `SFML` window creation.

