#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import os
import re

md_dir = os.path.join(os.getcwd(), 'docs/md/')

print('md_dir: {}'.format(md_dir))

if __name__ == '__main__':
    """
    This simply parses every .md files found where this script is executed
    and removes the extra `# group` or `{#group__}` strings's framgents that clutters the moxygen-erated documentation.
    """
    for f in os.listdir(md_dir):
        if os.path.isdir(f):
            continue

        if not f.endswith('.md'):
            print('Ignoring {}'.format(f))
            continue

        sanitized_content=''
        with open(os.path.join(md_dir, f), 'r') as md_read:
            sanitized_content=re.sub(r'({#group__.+)|({#class.+)|({#struct.+)|(# group .+)',
                                     '',
                                     md_read.read())
            # md_content.replace(sanitization_regex, '')

        with open(os.path.join(md_dir, f), 'w') as md_write:
            print('Sanitized {}'.format(f))
            md_write.write(sanitized_content)
