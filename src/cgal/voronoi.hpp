#pragma once

#include "../draw/color_gradient.hpp"

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Projection_traits_xy_3.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Delaunay_triangulation_adaptation_traits_2.h>
#include <CGAL/Delaunay_triangulation_adaptation_policies_2.h>
#include <CGAL/Voronoi_diagram_2.h>

#include <SFML/Graphics.hpp>
#include <iostream>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Projection_traits_xy_3<K> PJ;

typedef CGAL::Delaunay_triangulation_2<PJ> DT;
typedef CGAL::Delaunay_triangulation_adaptation_traits_2<DT> AT;
typedef CGAL::Delaunay_triangulation_caching_degeneracy_removal_policy_2<DT> AP;

typedef CGAL::Voronoi_diagram_2<DT, AT, AP> VD;

using namespace sf;

/** @addtogroup cgal
 *  @{*/

/** 
 * This class pipes input points to a `CGAL` [Voronoi](https://doc.cgal.org/latest/Voronoi_diagram_2/index.html#Chapter_2D_Voronoi_Diagram_Adaptor) diagram.
 * TODO -> Make 2 voronoi diagrams by adding an [Apollonius](https://doc.cgal.org/latest/Apollonius_graph_2/index.html#Chapter_2D_Apollonius_Graphs) graph.
 */
class Voronoi
{
public:
  /**
   * Constructor.
   * 
   * @param fill_gradient A ColorGradient instance used to map a ramp of colors to a 3rd value input
   */
  Voronoi(shared_ptr<ColorGradient> fill_gradient) : fill_gradient_(fill_gradient){};

  /** Destructor. */
  ~Voronoi();

  /**
   * Inserts a new point.
   * 
   * @param point A `sf::Vectorf2f` coordinate
   */
  void insert(const sf::Vector2f &point);

  /**
   * Draws the Voronoi diagram.
   * 
   * @param fill Whether to draw as outlines or filled areas
   */
  void draw(const bool fill = false);

private:
  /** A `vector` of `sf::Vector2f` holding points on which calculations are made. */
  vector<sf::Vector2f> points_;

  /** Shared pointer to a ColorGradient instance used to disseminate colors within the drawing. */
  shared_ptr<ColorGradient> fill_gradient_;

  /**
   * The `CGAL` voronoi diagram instance.
   * 
   * @see https://doc.cgal.org/latest/Voronoi_diagram_2/classCGAL_1_1Voronoi__diagram__2.html
   */
  VD voronoi_;

  /** Keeps track of the smallest face dimension. */
  float max_area_;

  /** Keeps track of the biggest face dimension. */
  float min_area_;

  /** Creates the Voronoi diagram, called internally before every `draw()`. */
  void compute_();
};
/**@}*/