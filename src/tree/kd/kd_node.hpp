/*
 * Copyright (c) 2015, Russell A. Brown
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSEARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* @(#)kdTreeSingleThread.cc	1.65 04/25/15 */

/*
 * The k-d tree was described by Jon Bentley in "Multidimensional Binary Search Trees
 * Used for Associative Searching", CACM 18(9): 509-517, 1975.  For k dimensions and
 * n elements of data, a balanced k-d tree is built in O(kn log n) + O((k+1)n log n)
 * time by first sorting the data in each of k dimensions, then building the k-d tree
 * in a manner that preserves the order of the k sorts while recursively partitioning
 * the data at each level of the k-d tree. No further sorting is necessary.  Moreover,
 * it is possible to replace the O((k+1)n log n) term with a O((k-1)n log n) term but
 * this approach sacrifices the generality of building the k-d tree for points of any
 * number of dimensions.
 */

#pragma once

#include <stdbool.h>
#include <stdlib.h>
#include <vector>
#include <list>
#include <iostream>

using namespace std;

/* One node of a k-d tree */
template <typename T>
class KdNode
{
  private:
    const T *tuple_;
    /** Space bounds within which the cut occurs. */
    Rect<T> *bbox_;
    KdNode<T> *lower_, *upper_;

  public:
    enum Type
    {
        ROOT,
        UPPER,
        LOWER
    };

    KdNode(const T *tuple, Rect<T> *bbox, const unsigned int _axis = 0, const unsigned int _depth = 0, const Type _type = ROOT)
        : tuple_(tuple), bbox_(bbox),
          axis(_axis), depth(_depth), type(_type)
    {
        lower_ = upper_ = nullptr;
    }

    ~KdNode()
    {
        delete tuple_;
        delete bbox_;
        delete lower_;
        delete upper_;
    }

    const unsigned int axis, depth;
    const Type type;

    const T *tuple() const { return tuple_; }

    Rect<T> *bbox() const { return bbox_; }
    void set_bbox(Rect<T> *bbox) { bbox_ = bbox; }

    KdNode<T> *upper() const { return upper_; }
    void set_upper(KdNode<T> *upper) { upper_ = upper; }

    KdNode<T> *lower() const { return lower_; }
    void set_lower(KdNode<T> *lower) { lower_ = lower; }

    /*
     * Search the k-d tree and find the KdNodes that lie within a cutoff distance
     * from a query node in all k dimensions.
     *
     * calling parameters:
     *
     * query - the query point
     * cut - the cutoff distance
     * dim - the number of dimensions
     * depth - the depth in the k-d tree
     *
     * returns: a list that contains the kdNodes that lie within the cutoff distance of the query node
     */
    list<KdNode> search_tree(const T *query, const T cut, const unsigned int dim, const unsigned int depth) const
    {
        // The partition cycles as x, y, z, w...
        const unsigned int axis = depth % dim;

        // If the distance from the query node to the k-d node is within the cutoff distance
        // in all k dimensions, add the k-d node to a list.
        list<KdNode> result;
        bool inside = true;
        for (unsigned int i = 0; i < dim; i++)
        {
            if (abs(query[i] - tuple_[i]) > cut)
            {
                inside = false;
                break;
            }
        }
        if (inside)
        {
            result.push_back(*this); // The push_back function expects a KdNode for a call by reference.
        }

        // Search the < branch of the k-d tree if the partition coordinate of the query point minus
        // the cutoff distance is <= the partition coordinate of the k-d node.  The < branch must be
        // searched when the cutoff distance equals the partition coordinate because the super key
        // may assign a point to either branch of the tree if the sorting or partition coordinate,
        // which forms the most significant portion of the super key, shows equality.
        if (lower_ != nullptr && (query[axis] - cut) <= tuple_[axis])
        {
            list<KdNode> lo_res = lower_->search_tree(query, cut, dim, depth + 1);
            result.splice(result.end(), lo_res); // Can't substitute search_tree(...) for lo_res.
        }

        // Search the > branch of the k-d tree if the partition coordinate of the query point plus
        // the cutoff distance is >= the partition coordinate of the k-d node.  The < branch must be
        // searched when the cutoff distance equals the partition coordinate because the super key
        // may assign a point to either branch of the tree if the sorting or partition coordinate,
        // which forms the most significant portion of the super key, shows equality.
        if (upper_ != nullptr && (query[axis] + cut) >= tuple_[axis])
        {
            list<KdNode> up_res = upper_->search_tree(query, cut, dim, depth + 1);
            result.splice(result.end(), up_res); // Can't substitute search_tree(...) for gt_res.
        }

        return result;
    }

    /*
     * Walk the k-d tree and check that the children of a node are in the correct branch of that node.
     *
     * calling parameters:
     *
     * dim - the number of dimensions
     * depth - the depth in the k-d tree 
     *
     * returns: a count of the number of kdNodes in the k-d tree
     */
    unsigned int verify_tree(const unsigned int dim, const unsigned int depth) const
    {
        unsigned count = 1;
        if (tuple_ == nullptr)
        {
            cout << "point is null!" << endl;
            exit(1);
        }

        // The partition cycles as x, y, z, w...
        const unsigned int axis = depth % dim;

        if (lower_ != nullptr)
        {
            if (lower_->tuple()[axis] > tuple_[axis])
            {
                cout << "child is > node!" << endl;
                exit(1);
            }
            if (super_key_compare_(lower_->tuple(), tuple_, axis, dim) >= 0)
            {
                cout << "child is >= node!" << endl;
                exit(1);
            }
            count += lower_->verify_tree(dim, depth + 1);
        }
        if (upper_ != nullptr)
        {
            if (upper_->tuple()[axis] < tuple_[axis])
            {
                cout << "child is < node!" << endl;
                exit(1);
            }
            if (super_key_compare_(upper_->tuple(), tuple_, axis, dim) <= 0)
            {
                cout << "child is <= node" << endl;
                exit(1);
            }
            count += upper_->verify_tree(dim, depth + 1);
        }
        return count;
    }

  private:
    /*
     * Initialize a reference array by creating references into the coordinates array.
     *
     * calling parameters:
     *
     * coordinates - a vector<T*> of pointers to each of the (x, y, z, w...) tuples
     * reference - a vector<T*> that represents one of the reference arrays
     */
    static void initialize_(vector<T *> &coordinates, vector<T *> &reference)
    {
        for (unsigned int j = 0; j < coordinates.size(); j++)
        {
            reference.at(j) = coordinates.at(j);
        }
    }

    /*
    * The super_key_compare_ method compares two T arrays in all k dimensions,
    * and uses the sorting or partition coordinate as the most significant dimension.
    *
    * calling parameters:
    *
    * a - a T*
    * b - a T*
    * p - the most significant dimension
    * dim - the number of dimensions
    *
    * returns: a T result of comparing two T arrays
    */
    static T super_key_compare_(const T *a, const T *b,
                                const unsigned int p, const unsigned int dim)
    {
        T diff = 0;
        for (unsigned int i = 0; i < dim; i++)
        {
            unsigned int r = i + p;
            // A fast alternative to the modulus operator for (i + p) < 2 * dim.
            r = (r < dim) ? r : r - dim;
            diff = a[r] - b[r];
            if (diff != 0)
            {
                break;
            }
        }
        return diff;
    }

    /*
     * The merge_sort_ function recursively subdivides the array to be sorted
     * then merges the elements. Adapted from Robert Sedgewick's "Algorithms
     * in C++" p. 166. Addison-Wesley, Reading, MA, 1992.
     *
     * calling parameters:
     *
     * reference - a vector<T*> that represents the reference array to sort
     * temporary - a temporary array into which to copy intermediate results;
     *             this array must be as large as the reference array
     * low - the start index of the region of the reference array to sort
     * high - the high index of the region of the reference array to sort
     * p - the sorting partition (x, y, z, w...)
     * dim - the number of dimensions
     * depth - the depth of subdivision
     */
    static void merge_sort_(vector<T *> &reference, vector<T *> &temporary,
                            const unsigned int low, const unsigned int high,
                            const unsigned int axis, const unsigned int dim)
    {
        unsigned int i, j, k;

        if (high > low)
        {
            // Avoid overflow when calculating the median.
            const unsigned int mid = low + ((high - low) >> 1);

            // Recursively subdivide the lower and upper halves of the array.
            merge_sort_(reference, temporary, low, mid, axis, dim);
            merge_sort_(reference, temporary, mid + 1, high, axis, dim);

            // Merge the results for this level of subdivision.
            for (i = mid + 1; i > low; i--)
            {
                temporary.at(i - 1) = reference.at(i - 1);
            }
            for (j = mid; j < high; j++)
            {
                temporary.at(mid + (high - j)) = reference.at(j + 1); // Avoid address overflow.
            }
            for (k = low; k <= high; k++)
            {
                reference.at(k) =
                    (super_key_compare_(temporary.at(i), temporary.at(j), axis, dim) < 0) ? temporary.at(i++) : temporary.at(j--);
            }
        }
    }

    /*
     * Check the validity of the merge sort and remove duplicates from a reference array.
     *
     * calling parameters:
     *
     * reference - a vector<T*> that represents one of the reference arrays
     * i - the leading dimension for the super key
     * dim - the number of dimensions
     *
     * returns: the end index of the reference array following removal of duplicate elements
     */
    static T remove_duplicates_(vector<T *> &reference,
                                const unsigned int i, const unsigned dim)
    {
        unsigned int end = 0;
        for (unsigned int j = 1; j < reference.size(); j++)
        {
            const T compare = super_key_compare_(reference.at(j), reference.at(j - 1), i, dim);
            if (compare < 0)
            {
                cout << "merge sort failure: super_key_compare_(ref[" << j << "], ref["
                     << j - 1 << "], (" << i << ") = " << compare << endl;
                exit(1);
            }
            else if (compare > 0)
            {
                reference.at(++end) = reference.at(j);
            }
        }
        return end;
    }

    static Rect<T> *cut_bbox_(KdNode<T> *node, const Type type)
    {
        const T *tuple = node->tuple();
        const T cut = node->axis ? tuple[1] : tuple[0];

        Rect<T> *bbox = new Rect<T>(*node->bbox());

        T right = bbox->left + bbox->width,
          bottom = bbox->top + bbox->height;

        if (type == UPPER)
        {
            if (node->axis)
            {
                bbox->height -= bottom - cut;
            }
            else
            {
                bbox->left = cut;
                bbox->width = right - cut;
            }
        }
        else if (type == LOWER)
        {
            if (node->axis)
            {
                bbox->height = bottom - cut;
                bbox->top = cut;
            }
            else
            {
                bbox->width -= right - cut;
            }
        }

        right = bbox->left + bbox->width;
        bottom = bbox->top + bbox->height;

        // cout << "[" << node->depth << " - " << node->axis << "]" << endl
        //      << "  -> X: " << bbox->left << " " << right << endl
        //      << "  -> Y: " << bbox->top << " " << bottom << endl
        //      << "  -> width: " << bbox->width << endl
        //      << "  -> height: " << bbox->height << endl;

        return bbox;
    }

    /*
     * This function builds a k-d tree by recursively partitioning the
     * reference arrays and adding kdNodes to the tree.  These arrays
     * are permuted cyclically for successive levels of the tree in
     * order that sorting occur on x, y, z, w...
     *
     * calling parameters:
     *
     * references - a vector< vector<T*> > of pointers to each of the (x, y, z, w...) tuples
     * temporary - a vector<T*> that is used as a temporary array
     * bbox - the bounding box of the node
     * start - start element of the reference arrays
     * end - end element of the reference arrays
     * dim - the number of dimensions
     * depth - the depth in the tree
     * type - a KdNode enum Type
     *
     * returns: a KdNode pointer to the root of the k-d tree
     */
    static KdNode *build_tree_(vector<vector<T *>> &references, vector<T *> &temporary, Rect<T> *bbox,
                               const unsigned int start, const unsigned int end,
                               const unsigned int dim, const unsigned int depth, const Type type = ROOT)
    {
        KdNode *node = nullptr;

        Rect<T> *lo_cut = nullptr;
        Rect<T> *up_cut = nullptr;

        // The axis permutes as x, y, z, w... and addresses the referenced data.
        const unsigned int axis = depth % dim;
        const unsigned int child_axis = (depth + 1) % dim;

        if (end == start)
        {
            // Only one reference was passed to this function, so add it to the tree.
            node = new KdNode<T>(references.at(0).at(end), bbox, axis, depth, type);
        }
        else if (end == start + 1)
        {
            // Two references were passed to this function in sorted order, so store the start
            // element at this level of the tree and store the end element as the > child.
            node = new KdNode<T>(references.at(0).at(start), bbox, axis, depth, type);

            up_cut = cut_bbox_(node, UPPER);
            node->set_upper(new KdNode<T>(references.at(0).at(end), up_cut, child_axis, depth + 1, UPPER));
        }
        else if (end == start + 2)
        {
            // Three references were passed to this function in sorted order, so
            // store the median element at this level of the tree, store the start
            // element as the < child and store the end element as the > child.
            node = new KdNode<T>(references.at(0).at(start + 1), bbox, axis, depth, type);

            up_cut = cut_bbox_(node, UPPER);
            node->set_upper(new KdNode<T>(references.at(0).at(end), up_cut, child_axis, depth + 1, UPPER));

            lo_cut = cut_bbox_(node, LOWER);
            node->set_lower(new KdNode<T>(references.at(0).at(start), lo_cut, child_axis, depth + 1, LOWER));
        }
        else if (end > start + 2)
        {
            // More than three references were passed to this function, so
            // the median element of references[0] is chosen as the tuple about
            // which the other reference arrays will be partitioned.  Avoid
            // overflow when computing the median.
            const int median = start + ((end - start) / 2);

            // Store the median element of references[0] in a new kdNode.
            node = new KdNode<T>(references.at(0).at(median), bbox, axis, depth, type);

            // Copy references[0] to the temporary array before partitioning.
            for (unsigned int i = start; i <= end; i++)
            {
                temporary.at(i) = references.at(0).at(i);
            }

            // Process each of the other reference arrays in a priori sorted order
            // and partition it by comparing super keys.  Store the result from
            // references[i] in references[i-1], thus permuting the reference
            // arrays.  Skip the element of references[i] that that references
            // a point that equals the point that is stored in the new k-d node.

            // at start =0, lower will be =-1 so it has to be signed int; aligned all others onto that type to avoid warnings.
            int lower = 0, upper = 0, lower_save = 0, upper_save = 0;
            for (unsigned int i = 1; i < dim; i++)
            {
                // Process one reference array.  Compare once only.
                lower = start - 1;
                upper = median;
                for (unsigned int j = start; j <= end; j++)
                {
                    const T compare = super_key_compare_(references.at(i).at(j), node->tuple(), axis, dim);
                    if (compare < 0)
                    {
                        references.at(i - 1).at(++lower) = references.at(i).at(j);
                    }
                    else if (compare > 0)
                    {
                        references.at(i - 1).at(++upper) = references.at(i).at(j);
                    }
                }

                // Check the new indices for the reference array.
                if (lower < (int)start || lower >= median)
                {
                    cout << "incorrect range for lower at depth = " << depth << " : start = "
                         << start << "  lower = " << lower << "  median = " << median << endl;
                    exit(1);
                }

                if (upper <= median || upper > (int)end)
                {
                    cout << "incorrect range for upper at depth = " << depth << " : median = "
                         << median << "  upper = " << upper << "  end = " << end << endl;
                    exit(1);
                }

                if (i > 1 && lower != lower_save)
                {
                    cout << " lower = " << lower << "  !=  lower_save = " << lower_save << endl;
                    exit(1);
                }

                if (i > 1 && upper != upper_save)
                {
                    cout << " upper = " << upper << "  !=  upper_save = " << upper_save << endl;
                    exit(1);
                }

                lower_save = lower;
                upper_save = upper;
            }

            // Copy the temporary array to references[dim-1] to finish permutation.
            for (unsigned int i = start; i <= end; i++)
            {
                references.at(dim - 1).at(i) = temporary.at(i);
            }

            // Recursively build the < branch of the tree.
            lo_cut = cut_bbox_(node, LOWER);
            node->set_lower(build_tree_(references, temporary, lo_cut, start, lower, dim, depth + 1, LOWER));

            // Recursively build the > branch of the tree.
            up_cut = cut_bbox_(node, UPPER);
            node->set_upper(build_tree_(references, temporary, up_cut, median + 1, upper, dim, depth + 1, UPPER));
        }
        else if (end < start)
        {

            // This is an illegal condition that should never occur, so test for it last.
            cout << "error has occurred at depth = " << depth << " : end = " << end
                 << "  <  start = " << start << endl;
            exit(1);
        }

        // Return the pointer to the root of the k-d tree.
        return node;
    }

  public:
    /*
     * The create_tree function performs the necessary initialization then calls the build_tree_ function.
     *
     * calling parameters:
     *
     * coordinates - a vector<T*> of references to each of the (x, y, z, w...) tuples
     * num_dimensions - the number of dimensions
     *
     * returns: a KdNode pointer to the root of the k-d tree
     */
    static KdNode *create_tree(vector<T *> &coordinates, const unsigned int num_dimensions, Rect<T> *root_space)
    {
        // Initialize and sort the reference arrays.
        vector<vector<T *>> references(num_dimensions, vector<T *>(coordinates.size()));
        vector<T *> temporary(coordinates.size());

        for (unsigned int i = 0; i < references.size(); i++)
        {
            initialize_(coordinates, references.at(i));
            merge_sort_(references.at(i), temporary, 0, references.at(i).size() - 1, i, num_dimensions);
        }

        // Remove references to duplicate coordinates via one pass through each reference array.
        vector<T> end(references.size());
        for (unsigned int i = 0; i < end.size(); i++)
        {
            end.at(i) = remove_duplicates_(references.at(i), i, num_dimensions);
        }

        // Check that the same number of references was removed from each reference array.
        for (unsigned int i = 0; i < end.size() - 1; i++)
        {
            for (unsigned int j = i + 1; j < end.size(); j++)
            {
                if (end.at(i) != end.at(j))
                {
                    cout << "reference removal error" << endl;
                    exit(1);
                }
            }
        }

        // Build the k-d tree.
        KdNode *root = build_tree_(references, temporary, root_space, 0, end.at(0), num_dimensions, 0);

        // Verify the k-d tree and report the number of KdNodes.
        root->verify_tree(num_dimensions, 0);

        // Return the pointer to the root of the k-d tree.
        return root;
    }

    /**
     * Get the amount of children's levels starting from a specified node.
     * 
     * @param node pointer to a `KdNode`
     * @return int deepness of the underlying levels
     */
    static unsigned deepness(const KdNode<T> *node)
    {
        unsigned count = 0;
        if (!node)
        {
            cerr << "node is null!" << endl;
        }

        if (node->lower())
        {
            count = max(deepness(node->lower()), node->depth);
        }
        if (node->upper())
        {
            count = max(deepness(node->upper()), node->depth);
        }

        return count;
    }

    static unsigned children_count(const KdNode<T> *node)
    {
        unsigned count = 1;
        if (!node)
        {
            cerr << "node is null!" << endl;
        }

        if (node->lower() != nullptr)
        {
            count += children_count(node->lower());
        }
        if (node->upper() != nullptr)
        {
            count += children_count(node->upper());
        }

        return count;
    }
};