#include "../utils/functions.hpp"
#include "gl_drawer.hpp"

#include <cmath>

void GLDrawer::draw_text(RenderTexture *render_texture,
                         const Vector2f &position,
                         const string &content,
                         const Font &font,
                         const unsigned short padding,
                         const unsigned short size,
                         const Text::Style style,
                         const Color &text_color,
                         const Color &bg_color)
{

    RectangleShape bg;
    bg.setFillColor(bg_color);

    Text text;
    text.setFont(font);
    text.setCharacterSize(size);
    text.setStyle(style);
    text.setFillColor(text_color);
    text.setString(content);

    const FloatRect bounds = Functions::extend_rect(text.getLocalBounds(), padding);
    // const Vector2f origin(bounds.left + bounds.width * .5f,
    //                       bounds.top + bounds.height * .5f);
    const Vector2f xy_padding(padding, padding);

    // text.setOrigin(origin);
    text.setPosition(position);

    bg.setSize({bounds.width, bounds.height});
    // bg.setOrigin(origin);
    bg.setPosition(position - xy_padding);

    render_texture->pushGLStates();
    render_texture->draw(bg);
    render_texture->draw(text);
    render_texture->popGLStates();
}

void GLDrawer::draw_junxion(const Junxion &junxion,
                            const Vector2f &radius,
                            GLStyle &style)
{
    glLineWidth(style.outline_width);

    // set_color(Color::White);

    set_color(style.fill_color);
    // const Vector2f texture{(float)junxion.point.texture.x, (float)junxion.point.texture.y};
    draw_circle(junxion.point.unit, radius, junxion.connections);

    style.fill_color.a = 75;
    set_color(style.fill_color);
    draw_circle(junxion.point.unit, radius * (float)junxion.connections, 32, GL_LINE_STRIP);

    style.fill_color.a = 155;
    set_color(style.fill_color);
    draw_circle(junxion.point.unit, radius * (float)junxion.connections, 32, GL_LINE_STRIP);

    /*
        glColor3f ( 0.f, 0.f, 0.f );
        draw_circle ( junxion.point.projection, radius, GL_LINE_STRIP, junxion.connections );
        glLineWidth ( style.outline_width );
        glColor3f ( 1.f, 0.f, 0.f );
        draw_circle ( junxion.point.projection, radius * ( 1+junxion.weight ), GL_LINE_STRIP );*/
}

void GLDrawer::draw_circle(const Vector2f &center,
                           const Vector2f &radius,
                           const unsigned int steps,
                           const GLenum gl_mode)
{
    glBegin(gl_mode);

    // cout << "draw circle: " << center.x << " " << center.y << endl
    //      << "radius: " << radius.x << endl;

    if (gl_mode == GL_TRIANGLE_FAN)
    {
        glVertex2f(center.x, center.y);
    }

    for (unsigned int i = 0; i <= steps; ++i)
    {
        const float angle = i * 2 * M_PI / steps - M_PI * .5;
        const float x = cos(angle) * radius.x;
        const float y = sin(angle) * radius.y;

        glVertex2f(center.x + x, center.y + y);
    }

    glEnd();
}

void GLDrawer::draw_rectangle(const Vector2f &top_left,
                              const Vector2f &bottom_right,
                              const GLenum gl_mode)
{
    glBegin(gl_mode);
    glVertex2f(top_left.x, top_left.y);
    glVertex2f(bottom_right.x, top_left.y);
    glVertex2f(bottom_right.x, bottom_right.y);
    glVertex2f(top_left.x, bottom_right.y);
    glEnd();
}

void GLDrawer::draw_cross(const Vector2f &center, const Vector2f &radius)
{
    glBegin(GL_LINES);

    float angle = -M_PI * .25;
    float x = cos(angle) * radius.x;
    float y = sin(angle) * radius.y;
    glVertex2f(center.x + x, center.y + y);

    angle = M_PI * .75;
    x = cos(angle) * radius.x;
    y = sin(angle) * radius.y;
    glVertex2f(center.x + x, center.y + y);

    angle = M_PI * .25;
    x = cos(angle) * radius.x;
    y = sin(angle) * radius.y;
    glVertex2f(center.x + x, center.y + y);

    angle = M_PI * 1.25;
    x = cos(angle) * radius.x;
    y = sin(angle) * radius.y;
    glVertex2f(center.x + x, center.y + y);

    glEnd();
}

void GLDrawer::draw_segment(const Vector2f &p1, const Vector2f &p2)
{
    glBegin(GL_LINES);
    glVertex2f(p1.x, p1.y);
    glVertex2f(p2.x, p2.y);
    glEnd();
}

void GLDrawer::draw_lines(const vector<Vector2f> &vertices, const GLenum gl_mode)
{
    glBegin(gl_mode);
    for (const auto &v : vertices)
    {
        glVertex2f(v.x, v.y);
    }
    glEnd();
}

void GLDrawer::draw_lines(const vector<Point> &vertices, const GLenum gl_mode)
{
    glBegin(gl_mode);
    for (const auto &v : vertices)
    {
        glVertex2f(v.unit.x, v.unit.y);
    }
    glEnd();
}

void GLDrawer::draw_triangles(const vector<Vector2f> &vertices, const GLenum gl_mode)
{
    glBegin(gl_mode);
    for (const auto &v : vertices)
    {
        glVertex2f(v.x, v.y);
    }
    glEnd();
}

//
// void GLDrawer::draw_quads ( const vector<Edge>& edges, const float width,
//                             const ColorGradient color_gradient, const GradientType gradient_type )
// {
//     unsigned int i = 0;
//
//     Color start_color;
//     Color end_color;
//
//     glBegin ( GL_QUADS );
//     for ( vector<Edge>::const_iterator it = edges.begin(); it != edges.end(); ++it ) {
//
//         if ( gradient_type == GradientType::PER_LINE ) {
//             start_color = color_gradient.color_at ( 0.f );
//             end_color = color_gradient.color_at ( 1.f );
//         } else if ( gradient_type == GradientType::PER_WEIGHT ) {
//             start_color = color_gradient.color_at ( it->p1.z );
//             end_color = color_gradient.color_at ( it->p2.z );
//         }
//
//         const float angle = atan2 ( it->p1.y - it->p2.y, it->p1.x - it->p2.x );
//
//         const Vector2f tl = Vector2f ( it->p1.x + cos ( angle - M_PI_2 ) * width,
//                                        it->p1.y + sin ( angle - M_PI_2 ) * width );
//
//         const Vector2f tr = Vector2f ( it->p1.x + cos ( angle + M_PI_2 ) * width,
//                                        it->p1.y + sin ( angle + M_PI_2 ) * width );
//
//         const Vector2f bl = Vector2f ( it->p2.x + cos ( angle + M_PI_2 ) * width,
//                                        it->p2.y + sin ( angle + M_PI_2 ) * width );
//
//         const Vector2f br = Vector2f ( it->p2.x + cos ( angle - M_PI_2 ) * width,
//                                        it->p2.y + sin ( angle - M_PI_2 ) * width );
//
//         glBegin ( GL_QUADS );
//         glColor3f ( start_color.r / 255.f, start_color.g / 255.f, start_color.b / 255.f );
//         glVertex2f ( tl.x, tl.y );
//         glVertex2f ( tr.x, tr.y );
//         glColor3f ( end_color.r / 255.f, end_color.g / 255.f, end_color.b / 255.f );
//         glVertex2f ( bl.x, bl.y );
//         glVertex2f ( br.x, br.y );
//         glEnd();
//
//         ++i;
//     }
// }
