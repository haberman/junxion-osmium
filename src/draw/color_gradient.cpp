#include "color_gradient.hpp"

#include <algorithm>

ColorGradient::ColorGradient(const std::vector<sf::Color> &colors)
{
    if (!ramp_.empty())
    {
        unsigned short i = 0;

        for (const auto &point : ramp_)
        {
            add_color_point(point.color, float(i) / float(ramp_.size() - 1));
            ++i;
        }
    }
}

ColorGradient::ColorGradient(const GradientStyle &gradient_type)
{
    create(gradient_type);
}

void ColorGradient::add_color_point(const sf::Color &color, const float position)
{
    for (unsigned int i = 0; i < ramp_.size(); i++)
    {
        if (position < ramp_[i].position)
        {
            ramp_.insert(ramp_.begin() + i, ColorPoint(color, position));
            return;
        }
    }
    ramp_.push_back(ColorPoint(color, position));
}

void ColorGradient::clear_gradient()
{
    ramp_.clear();
}

void ColorGradient::create(const GradientStyle &gradient_type)
{
    clear_gradient();

    switch (gradient_type)
    {
    case Wiretap:
        ramp_.push_back(ColorPoint(Color(0x8A2387ff), .0f));
        ramp_.push_back(ColorPoint(Color(0xE94057ff), .5f));
        ramp_.push_back(ColorPoint(Color(0xF27121ff), 1.f));
        break;
    case Rainbow:
        ramp_.push_back(ColorPoint(Color(0x0000ffff), .0f));  // Blue.
        ramp_.push_back(ColorPoint(Color(0x00ffffff), .25f)); // Cyan.
        ramp_.push_back(ColorPoint(Color(0x00ff00ff), .5f));  // Green.
        ramp_.push_back(ColorPoint(Color(0xffff00ff), .75f)); // Yellow.
        ramp_.push_back(ColorPoint(Color(0xff0000ff), 1.f));  // Red.
        break;
    case Mango:
        ramp_.push_back(ColorPoint(Color(0xffffe259), .0f));
        ramp_.push_back(ColorPoint(Color(0xffffa751), 1.f));
        break;
    case RedSunset:
        ramp_.push_back(ColorPoint(Color(0xff355c7d), .0f));
        ramp_.push_back(ColorPoint(Color(0xff6c5b7b), .5f));
        ramp_.push_back(ColorPoint(Color(0xffc06c84), 1.f));
        break;
    case RelaxingRed:
        ramp_.push_back(ColorPoint(Color(0xfffbd5ff), .0f));
        ramp_.push_back(ColorPoint(Color(0xb20a2cff), 1.f));
        break;
    case SandBlue:
        ramp_.push_back(ColorPoint(Color(0x3e5151ff), .0f));
        ramp_.push_back(ColorPoint(Color(0xdecba4ff), 1.f));
        break;
    case Shifter:
        ramp_.push_back(ColorPoint(Color(0xbc4e9cff), .0f));
        ramp_.push_back(ColorPoint(Color(0xf80759ff), 1.f));
        break;
    case Windy:
        ramp_.push_back(ColorPoint(Color(0xacb6e5ff), .0f));
        ramp_.push_back(ColorPoint(Color(0x86fde8ff), 1.f));
        break;
    case WeddingDayBlues:
        ramp_.push_back(ColorPoint(Color(0x40e0d0ff), .0f));
        ramp_.push_back(ColorPoint(Color(0xff8c00ff), .5f));
        ramp_.push_back(ColorPoint(Color(0xff0080ff), 1.f));
        break;
    }
}

void ColorGradient::set_distribution(const vector<float>& dist)
{
    if (dist.size() != ramp_.size())
    {
        return;
    }

    for (unsigned i = 0; i < dist.size(); ++i)
    {
        ramp_[i].position = dist[i];
    }
}

const Color ColorGradient::color_at(const float position) const
{
    if (ramp_.size() == 0)
    {
        return Color::White;
    }

    Color c;

    unsigned i = 0;
    for (const ColorPoint &point : ramp_)
    {
        if (position < point.position)
        {
            const ColorPoint prev_point = ramp_[max(0, (int)i - 1)];

            const float pos_diff = prev_point.position - point.position;
            const float frac_between = (pos_diff == 0) ? 0 : (position - point.position) / pos_diff;

            c.r = point.color.r + (prev_point.color.r - point.color.r) * frac_between;
            c.g = point.color.g + (prev_point.color.g - point.color.g) * frac_between;
            c.b = point.color.b + (prev_point.color.b - point.color.b) * frac_between;

            return c;
        }
        ++i;
    }

    return ramp_.back().color;
}

const vector<GLfloat> ColorGradient::gl_color_at(const float position) const
{
    const Color color = color_at(position);
    vector<GLfloat> gl_color;

    const GLfloat red = (float)color.r / 255.f;
    gl_color.push_back(red);

    const GLfloat green = (float)color.g / 255.f;
    gl_color.push_back(green);

    const GLfloat blue = (float)color.b / 255.f;
    gl_color.push_back(blue);

    return gl_color;
}

size_t ColorGradient::size() const
{
    return ramp_.size();
}
