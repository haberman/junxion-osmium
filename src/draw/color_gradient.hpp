#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>

#include <iostream>

using namespace std;
using namespace sf;

/** @addtogroup draw
 *  @{
 */

/**
  * Some gradient presets picked over [here](https://uigradients.com/).
  */
enum GradientStyle
{
  Wiretap,        /**< Deep violet to orange. [link](https://uigradients.com/#Wiretap) */
  Rainbow,        /**< 5 steps RGB cycle. */
  Mango,          /**< Light sunrise yellow. [link](https://uigradients.com/#Mango) */
  RedSunset,      /**< Late sunset blue. [link](https://uigradients.com/#RedSunset) */
  RelaxingRed,    /**< Bright yellow to pomegranate red. ([link]https://uigradients.com/#Relaxingred) */
  SandBlue,       /**< Desaturated dark blue to yellow. [link](https://uigradients.com/#SandtoBlue) */
  Shifter,        /**< Purple to pink. [link](https://uigradients.com/#Shifter) */
  Windy,          /**< Bright purple to icy blue. [link](https://uigradients.com/#Windy) */
  WeddingDayBlues /**< Cyan-orange-pink. [link](https://uigradients.com/#WeddingDayBlues) */
};

/**
 * This class offers methods to interact with gradients as made by:
 * - a list of colors;
 * - an interpolation function that translates a one-dimensional position to RGB.
 */
class ColorGradient
{
public:
  /**
   * Default constructor could use any hard-coded value of the `GradientStyle` enum.
   * @param style One of the `GradientStyle` member
   */
  ColorGradient(const GradientStyle &style = Mango);

  /**
   * Constructor that accepts a user provided list of colors.
   * @param colors Reference to a `vector` of `sf::Color`
   */
  ColorGradient(const vector<sf::Color> &colors);

  /** Destructor */
  ~ColorGradient() {}

  /**
   * Inserts a new color point into its correct position.
   * 
   * @param color     The `sf::Color` to insert
   * @param position  Where to insert the color in the ramp
   */
  void add_color_point(const Color &color, const float position);

  /** Clears the gradient colors vector. */
  void clear_gradient();

  /** Places a default color heapmap gradient into the `color` vector. */
  void create(const GradientStyle &gradient_type);

  /** 
   * Updates the distribution of colors.
   * 
   * @param distribution The new gradient's distribution list.
   **/
  void set_distribution(const vector<float> &distribution);

  /**
   * Returns the color at the given position.
   * 
   * @param position Where the color should be computed.
   * @return `sf::Color`.
   */
  const Color color_at(const float position) const;

  /**
   * Returns the color at the given position.
   * 
   * @param position Where the color should be computed.
   * @return `vector` of RGBA `GLfloat`.
   */
  const vector<GLfloat> gl_color_at(const float position) const;

  /** Returns the amount of colors within the gradient ramp. */
  size_t size() const;

private:
  /** Internal `struct` used to store a color stop inside the ColorGradient. */
  struct ColorPoint
  {
    /** The color */
    Color color;

    /** Position of the color along the gradient (between 0 and 1). */
    float position;

    /**
     * Constructor.
     * @param _color    `sf::Color` of this stop
     * @param _position Position of this stop in the gradient
     */
    ColorPoint(Color _color, float _position) : color(_color),
                                                position(_position) {}
  };

  /** The gradent ramp as a `vector` of color points in ascending value. */
  vector<ColorPoint> ramp_;
};
