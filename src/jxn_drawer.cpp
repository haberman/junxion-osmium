#include <cstdlib>  // for exit
#include <fstream>  // for loading geojson
#include <iostream> // for cout

#include <chrono>  // chrono::system_clock
#include <iomanip> // put_time
#include <sstream> // stringstream

#include <sys/stat.h> // for mkdir()

// JsonCPP
#include <json/json.h>

// OSMIUM
#include <osmium/geom/mercator_projection.hpp>

// SFML
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <SFML/System.hpp>

// PROJECT
#include "utils/functions.hpp"
#include "utils/projector.hpp"

#include "io/data_buffer.hpp"
#include "io/input_parser.hpp"

#include "draw/color_gradient.hpp"
#include "draw/gl_drawer.hpp"

#include "cgal/triangulator.hpp"
#include "cgal/voronoi.hpp"

#include "tree/kd/kd_tree.hpp"

using namespace sf;

/** @addtogroup drawer
 *  @{*/

// @{
/**
 * Self describing application constants.
 * TODO -> move this into a configuration file (and don't create a dependency.) */
const string ASSET_MONOID_FONT = "../assets/fonts/Monoid_Regular.ttf";

const string OUTPUT_DIRECTORY = "../output";
const string OUTPUT_MODE = "file";
// 300dpi...
// A3-> 3508 pixels x 4961
// A4-> StaTors.Net
const unsigned short OUTPUT_WIDTH = 1400;

const float MARKER_RADIUS = 3.f;
const float PROJECTION_PADDING = 10.f;
const float FONT_SIZE = 9.f;

const Color CLEAR_COLOR = Color::White;
const Color FRONT_COLOR = Color::Black;
// @}

/**
 * A simple method that caps a width up to the screen resolution when mode is `screen`.
 * 
 * @param width The width to check
 * @param mode  The output mode (facilitates the single use we have for this function inside the CLI `main` callback)
 * @return The clamped width.
 */
unsigned int output_width_(const unsigned int width, const string &mode)
{
    if (mode != "screen")
    {
        return width;
    }

    const VideoMode video_mode = VideoMode::getDesktopMode();
    return min(width, video_mode.width);
}

/**
 * Extracts a file's basename from a given path.
 * 
 * @param path The input path to inspect
 * @param ext  Whether we should include the file extension as well
 * @return The file's basename.
 */
string basename(const string &path, const bool ext = true)
{
    string filename = path;

    const size_t last_slash_idx = path.find_last_of("\\/");
    if (string::npos != last_slash_idx)
    {
        filename.erase(0, last_slash_idx + 1);
    }

    if (!ext)
    {
        const size_t period_idx = filename.rfind('.');
        if (string::npos != period_idx)
        {
            filename.erase(period_idx);
        }
    }

    return filename;
}

/**
 * Fills the DataBuffer with junxions coming from a given GeoJSON file.
 * The rule is the following: junxion comes as a `GeometryCollection` and contains;
 * - a single `Point` which represents the junxion's node location;
 * - one or more adjacents as a `MultilineString` list of locations.
 * 
 * @param geojson_file    The input file
 * @param bbox            Pointer to a `sf::Box` extended to include spatial extent of input data
 * @param min_connections Reference to track the minimum of junxions' connections
 * @param max_connections Reference to track the maximum of junxions' connection.
 */
void parse_junxions(const string geojson_file, Box *bbox,
                    float &min_connections, float &max_connections)
{
    Json::CharReaderBuilder builder;
    builder["collectComments"] = false;
    Json::Value geojson;

    /** Feed the reader with an input stream */
    ifstream stream(geojson_file);
    bool ok = parseFromStream(builder, stream, &geojson, nullptr);
    // reader.parse(stream, geojson);

    if (!ok)
    {
        cerr << "[ERROR] Failed at parsing geojson file: " << geojson_file << endl;
        return;
    }

    /** The root GeoJSON features list */
    const Json::Value &features = geojson["features"];

    for (unsigned int i = 0; i < features.size(); ++i)
    {
        /** We're at junxion's level. */
        const Json::Value &geometries = features[i]["geometry"]["geometries"];
        const Json::Value &properties = features[i]["properties"];

        const Json::UInt connections = properties["connections"].asUInt();
        const Json::UInt64 uid = properties["uid"].asUInt64();

        for (unsigned short j = 0; j < geometries.size(); ++j)
        {
            const Json::Value &geometry = geometries[j];

            /** The type is a `Point` -> we store a new Junxion instance in the DataBuffer singleton. */
            if (geometry["type"].asString() == "Point")
            {
                const float loc_lng = geometry["coordinates"][0].asFloat();
                const float loc_lat = geometry["coordinates"][1].asFloat();

                Junxion junxion(uid, Location(loc_lng, loc_lat));
                junxion.connections = connections;
                bbox->extend(junxion.point.location);

                DataBuffer::get_instance()->junxions[uid] = junxion;
            }
            /** The type is a `MultiLineString` -> we append points on either the `after` or `before` list of junxion's adjacents. */
            else if (geometry["type"].asString() == "LineString")
            {
                const Json::Value &multiline = geometry["coordinates"];
                const Json::Value &way_uid = geometry["properties"]["uid"];
                const Json::Value &side = geometry["properties"]["side"];

                Adjacent adjacent;
                adjacent.way_uid = way_uid.asInt64();

                for (unsigned short k = 0; k < multiline.size(); ++k)
                {
                    Point point;
                    point.location = Location(multiline[k][0].asFloat(),
                                              multiline[k][1].asFloat());

                    if (side.asString() == "before")
                    {
                        adjacent.before.push_back(point);
                    }
                    else if (side.asString() == "after")
                    {
                        adjacent.after.push_back(point);
                    }

                    DataBuffer::get_instance()->junxions[uid].adjacents[adjacent.way_uid] = adjacent;

                    // for (unsigned short l = 0; l < adjacent_line.size(); ++l)
                    // {
                    //     Point point;
                    //     point.location = Location(adjacent_line[l][0].asFloat(),
                    //                               adjacent_line[l][1].asFloat());

                    //     if (side.asString() == "lower")
                    //     {
                    //         adjacent.before.push_back(point);
                    //     }
                    //     else if (side.asString() == "upper")
                    //     {
                    //         adjacent.after.push_back(point);
                    //     }

                    //     DataBuffer::get_instance()->junxions[uid].adjacents[adjacent.way_uid] = adjacent;
                    // }
                }
            }
        }

        /** Updates the min/max amount of junxions' connections. */
        min_connections = min(min_connections, (float)connections);
        max_connections = max(max_connections, (float)connections);
    }
}

/**
 * Main program entry point.
 * This is a single threaded logic flow from input parsing to output drawing, OpenGL context or `SFML` window creation.
 * `CGAL` computation and drawing are made within the corresponding package while the `map` is drawn within this method.
 * See [source code](https://framagit.org/haberman/junxion-osmium/blob/master/src/jxn_drawer.cpp#L193) for more comments.
 */
int main(int argc, char **argv)
{
    /** Configures the InputParser. */
    InputParser input(argc, argv);
    if (input.argument_exists("-h"))
    {
        cout << "junxion-drawer 0.5" << endl
             << endl;
        cout << "usage: jxn_drawer [options] junxions.geojson" << endl;
        cout << "_______" << endl;
        cout << "OPTIONS" << endl
             << endl;
        cout << "* generic *" << endl;
        cout << "  -h\tshow this help message and exit;" << endl;
        cout << "  -v\tincrease verbosity." << endl;
        cout << "* output *" << endl;
        cout << "  -w\twidth in pixels, used to create projection space [default: " << OUTPUT_WIDTH << "];" << endl;
        cout << "  -m\tmode, either: 'screen' or 'file' [default: " << OUTPUT_MODE << "];" << endl;
        cout << "  -o\tdirectory to save images in 'file' mode [default: " << OUTPUT_DIRECTORY << "]." << endl;
        cout << "* drawing *" << endl;
        cout << "  -M\tmap projection;" << endl;
        cout << "  -K\tK(2)d spatial tree;" << endl;
        cout << "  -T\ttriangulation;" << endl;
        cout << "  -V\tvoronoi diagrams." << endl;

        return EXIT_SUCCESS;
    }

    const string geojson_input = input.get_argument();

    /** Common rejections. */
    if (geojson_input.find(".geojson") == string::npos)
    {
        cout << "[ERROR] Missing required geojson file input." << endl;
        return EXIT_FAILURE;
    }

    if (access(geojson_input.c_str(), F_OK) == -1)
    {
        cout << "[ERROR] File " << geojson_input << " does not exists." << endl;
        return EXIT_FAILURE;
    }

    /** Arguments setup. */
    const string name = basename(geojson_input);
    const bool verbose = input.get_argument("-v", false);

    const string output_mode = input.get_argument_s("-m", OUTPUT_MODE);
    const unsigned int output_width = output_width_(input.get_argument("-w", OUTPUT_WIDTH), output_mode);
    const string output_directory = input.get_argument_s("-o", OUTPUT_DIRECTORY);

    const bool draw_map = input.get_argument("-M", false);
    const bool draw_kdtree = input.get_argument("-K", false);
    const bool draw_triangle = input.get_argument("-T", false);
    const bool draw_voronoi = input.get_argument("-V", false);

    if (verbose)
    {
        cout << "Reading geojson features..." << endl;
    }

    /** Data parsing and screen (eg. drawing) projection setup. */
    DataBuffer *data_buffer = DataBuffer::get_instance();

    /** Font setup */
    Font *monoid_font = new Font;
    if (!monoid_font->loadFromFile(ASSET_MONOID_FONT))
    {
        cout << "[ERROR] can't load font asset." << endl;
        return EXIT_FAILURE;
    }

    data_buffer->monoid_font = monoid_font;

    Box bbox;
    float min_connections = 100.f;
    float max_connections = 0.f;
    parse_junxions(geojson_input, &bbox, min_connections, max_connections);

    const geom::Coordinates merc_tr = geom::lonlat_to_mercator(bbox.top_right());
    const geom::Coordinates merc_bl = geom::lonlat_to_mercator(bbox.bottom_left());
    const double merc_width = abs(merc_tr.x - merc_bl.x);
    const double merc_height = abs(merc_tr.y - merc_bl.y);
    data_buffer->mercator_space = new FloatRect(merc_bl.x, merc_bl.y, merc_width, merc_height);

    const int output_height = output_width * (data_buffer->mercator_space->width / data_buffer->mercator_space->height);
    data_buffer->texture_space = new IntRect(0, 0, output_width, output_height);
    data_buffer->pixel = new Vector2f(2.f / (float)data_buffer->texture_space->width, 2.f / (float)data_buffer->texture_space->height);

    data_buffer->radius = MARKER_RADIUS;

    /** Creates the rendering target that is common to both `screen` and `file` output. */
    sf::RenderTexture render_texture;

    if (!render_texture.create(output_width, output_height))
    {
        cout << "[ERROR] can't create texture." << endl;
        return EXIT_FAILURE;
    }

    /** Creates new instance(s) of drawing class. */
    shared_ptr<ColorGradient> theme_gradient = make_shared<ColorGradient>(GradientStyle::WeddingDayBlues);
    theme_gradient->set_distribution({.0f, .6f, 1.f});

    Triangulator *triangulator = nullptr;
    Voronoi *voronoi = nullptr;
    KdTree<float> *kd_tree = nullptr;

    if (verbose)
    {
        cout << "Drawing " << data_buffer->junxions.size() << " junxions "
             << "to a [" << data_buffer->texture_space->width << "," << data_buffer->texture_space->height << "] pixel space as:" << endl;

        if (draw_map)
        {
            cout << "  -> map" << endl;
        }

        if (draw_triangle)
        {
            cout << "  -> triangles" << endl;
            triangulator = new Triangulator(theme_gradient);
        }

        if (draw_kdtree)
        {
            cout << "  -> kdtree" << endl;
            kd_tree = new KdTree<float>(theme_gradient);
        }

        if (draw_voronoi)
        {
            cout << "  -> voronoi" << endl;
            voronoi = new Voronoi(theme_gradient);
        }
    }

    /** Setup OpenGL viewport. */
    if (!render_texture.setActive(true))
    {
        cout << "[ERROR] can't activate texture." << endl;
        return EXIT_FAILURE;
    }

    glViewport(0, 0, data_buffer->texture_space->width, data_buffer->texture_space->height);
    glClearColor(CLEAR_COLOR.r / 255, CLEAR_COLOR.g / 255, CLEAR_COLOR.b / 255, CLEAR_COLOR.a / 255);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);

    Text jxn_info;
    jxn_info.setFont(*data_buffer->monoid_font);
    jxn_info.setCharacterSize(FONT_SIZE);

    /**
     * Junxions' projection
     * Calculates the projection of every locations and inserts points to `draw` modules if requested.
     * TODO: -> watch out for mixture of padded / non padded coordinates.
     */
    for (auto &j : data_buffer->junxions)
    {
        j.second.weight = Functions::map_number(j.second.connections, min_connections, max_connections, 0.f, 1.f);

        j.second.point.mercator = geom::lonlat_to_mercator(j.second.point.location);
        j.second.point.unit = Projector::mercator<float>(j.second.point.mercator, PROJECTION_PADDING * data_buffer->pixel->x);
        j.second.point.texture = Projector::mercator<int>(j.second.point.mercator, (int)PROJECTION_PADDING);

        for (auto &a : j.second.adjacents)
        {
            for (auto &before : a.second.before)
            {
                before.mercator = geom::lonlat_to_mercator({before.location});
                before.unit = Projector::mercator<float>(before.mercator, PROJECTION_PADDING * data_buffer->pixel->x);
                before.texture = Projector::mercator<int>(before.mercator, (int)PROJECTION_PADDING);
            }

            for (auto &after : a.second.after)
            {
                after.mercator = geom::lonlat_to_mercator({after.location});
                after.unit = Projector::mercator<float>(after.mercator, PROJECTION_PADDING * data_buffer->pixel->x);
                after.texture = Projector::mercator<int>(after.mercator, (int)PROJECTION_PADDING);
            }
        }

        /** Tree drawing depends on `SFML` text drawing engine, so we use the output coordinate system. */
        if (draw_kdtree)
        {
            kd_tree->insert(j.second.point.unit);
        }

        if (draw_voronoi)
        {
            voronoi->insert(j.second.point.unit);
        }

        if (draw_triangle)
        {
            triangulator->insert(j.second.point.unit);
        }
    }

    /** Effective drawings. */
    if (draw_triangle)
    {
        triangulator->draw();
    }

    if (draw_voronoi)
    {
        voronoi->draw();
    }

    if (draw_kdtree)
    {
        kd_tree->draw(&render_texture);
    }

    /**
     * Map drawing built on top of `SFML`.
     * This will loop over the junxions data again as it requires more intricated computation.
     */
    if (draw_map)
    {
        Text text;
        text.setFont(*data_buffer->monoid_font);
        text.setCharacterSize(FONT_SIZE);
        text.setStyle(Text::Regular);
        text.setFillColor(FRONT_COLOR);

        RectangleShape bg;
        bg.setFillColor(CLEAR_COLOR);
        // render_texture.setActive(true);
        /** Junxions map iterator */
        // unsigned i = 0;
        for (const auto &j : data_buffer->junxions)
        {
            // if (i > 10)
            // {
            //     break;
            // }

            /** Draws adjacents. */
            // glColor3f(1.f, 1.f, 1.f);
            GLDrawer::set_color(FRONT_COLOR);
            for (const auto &a : j.second.adjacents)
            {
                GLDrawer::draw_lines(a.second.before, GL_LINES);
                GLDrawer::draw_lines(a.second.after, GL_LINES);
            }

            // only draw details for which there is 4 junxions at least.
            if (j.second.connections < 4)
            {
                continue;
            }

            /** Setup styling. */
            GLStyle style;
            style.fill_color = theme_gradient->color_at(j.second.weight);
            style.outline_color = theme_gradient->color_at(j.second.weight);

            /** Draws the main junxion. */

            GLDrawer::draw_junxion(j.second, data_buffer->pixel_radius(), style);
            // GLDrawer::draw_circle(position, data_buffer->pixel_radius(), j.second.connections);
            // render_texture.pushGLStates();
            // render_texture.popGLStates();

            stringstream ss;
            ss << j.second.point.uid;
            jxn_info.setString(ss.str());
            text.setString(ss.str());

            const Vector2f position = Vector2f(j.second.point.texture.x,
                                               data_buffer->texture_space->height - j.second.point.texture.y);
            const FloatRect bounds = Functions::extend_rect(text.getLocalBounds(), PROJECTION_PADDING);
            // pixel snapping text positionning
            const Vector2f origin(round(bounds.left + bounds.width * .5f),
                                  round(bounds.top + bounds.height * .5f - MARKER_RADIUS));

            text.setOrigin(origin);
            text.setPosition(position);

            const Vector2f xy_padding(PROJECTION_PADDING, PROJECTION_PADDING);
            bg.setSize({bounds.width, bounds.height});
            bg.setOrigin(origin);
            bg.setPosition(position - xy_padding);

            render_texture.pushGLStates();
            // render_texture.draw(bg);
            render_texture.draw(text);
            render_texture.popGLStates();
            // GLDrawer::draw_text(&render_texture,
            //                     position, ss.str(),
            //                     *DataBuffer::get_instance()->monoid_font,
            //                     PROJECTION_PADDING, FONT_SIZE,
            //                     Text::Regular, Color::White, Color::Transparent);

            // ++i;
        }
    }

    /** `screen` output mode requires a window, it's made closable by pressing Esc. */
    if (output_mode == "screen")
    {
        Sprite sprite(render_texture.getTexture());
        sprite.setTextureRect(IntRect(0, data_buffer->texture_space->height,
                                      data_buffer->texture_space->width, -data_buffer->texture_space->height));

        const VideoMode video_mode = VideoMode::getDesktopMode();

        const Vector2i window_position = Vector2i((video_mode.width - data_buffer->texture_space->width) * .5f,
                                                  (video_mode.height - data_buffer->texture_space->height) * .5f);

        ContextSettings context_settings;
        context_settings.depthBits = 24;
        context_settings.antialiasingLevel = 6;

        RenderWindow window(video_mode, "JUNXION", Style::Default, context_settings);
        window.setSize(Vector2u(data_buffer->texture_space->width, data_buffer->texture_space->height));
        window.setPosition(window_position);
        window.setActive(true);

        while (window.isOpen())
        {
            Event event;

            while (window.pollEvent(event))
            {
                if (event.type == Event::Closed)
                {
                    window.close();
                }
                else if (event.type == Event::Resized)
                {
                    glViewport(0, 0, event.size.width, event.size.height);
                }
                else if (event.type == Event::KeyPressed)
                {
                    if (event.key.code == Keyboard::Escape)
                    {
                        window.close();
                    }
                }
            }
            window.draw(sprite);
            window.display();
        }
    }

    /**
     * `file` output mode saves drawing in a png.
     * The filename is simply stamped with the time of creation. 
     */
    else if (output_mode == "file")
    {
        auto now = chrono::system_clock::now();
        auto now_time_t = chrono::system_clock::to_time_t(now);

        mkdir(output_directory.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

        stringstream ss;
        ss << output_directory << "/"
           << put_time(localtime(&now_time_t), "drawer_%Y-%m-%d_%X.png");

        Image texture_export = render_texture.getTexture().copyToImage();
        texture_export.flipVertically(); // this is done by the `setTextureRect` method for the screen mode
        texture_export.saveToFile(ss.str());

        if (verbose)
        {
            cout << "Image saved as: " << ss.str() << endl;
        }
    }

    data_buffer->destroy_instance();
    return EXIT_SUCCESS;
}
/**@}*/