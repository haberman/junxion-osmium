#pragma once

#include <SFML/Graphics.hpp>

using namespace std;
using namespace sf;

// Turns out this is a good place to define the modules' documentation's hierarchy

/** @defgroup io     input/output module
 *  @defgroup draw   2D drawing module
 *  @defgroup cgal   `CGAL` geometric computation module
 *  @defgroup parser OSM data parser CLI
 *  @defgroup drawer GeoJSON data parser & visualizer CLI
 */

/**
 * Static-only class that exposes divers utilities.
 */
class Functions
{
public:
  template <class T>
  static const bool is_unique(vector<T> &x)
  {
    sort(x.begin(), x.end());
    return adjacent_find(x.begin(), x.end()) == x.end();
  }

  /**
   * Linear 1D translation of a number from an input range to an other.
   * 
   * @param in      The input number to translate
   * @param in_min  Lower input boudary
   * @param in_max  Upper input boudary
   * @param out_min Target lower bounadry
   * @param out_max Target upper boundary
   * @return float  Interpolated result.
   */
  static const float map_number(const float in,
                                const float in_min, const float in_max,
                                const float out_min, const float out_max)
  {
    float slope = (out_max - out_min) / (in_max - in_min);
    return out_min + slope * (in - in_min);
  }

  /**
   * Extends an input `sf::FloatRect` by a given amount of padding pixels.
   * 
   * @param rect    The incoming rectangle
   * @param padding Value used to grow / shrink the output rectangle
   * @return `FloatRect` with new dimensions.
   */
  static const FloatRect extend_rect(const FloatRect &rect, const float padding)
  {
    const FloatRect extent = FloatRect(rect.left - padding,
                                       rect.top - padding,
                                       rect.width + 2.f * padding,
                                       rect.height + 2.f * padding);
    return extent;
  }
};