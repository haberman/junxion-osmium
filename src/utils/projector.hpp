#pragma once

#include "../io/data_buffer.hpp"
#include "../utils/functions.hpp"

#include <osmium/geom/coordinates.hpp>
#include <SFML/Graphics.hpp>

using namespace std;
using namespace osmium;
using namespace sf;

/** @notice
    Projected bounds:
        -20026376.39 -20048966.10
        20026376.39 20048966.10

    WGS84 bounds:
        -180.0 -85.06
        180.0 85.06 */

class Projector
{
  public:
    /**
     * Project a mercator geographic coordinate into a given output space.
     * It internally uses `DataBuffer::xxxxx_space`s, so those pointers need to be set.
     * 
     * @param location The input coordinates
     * @param padding  An optionnal inset space to frame the projection
     * @param space    The output space (either `UNIT` or `TEXTURE`) 
     * 
     * @return The `sf::Vector2<T>` projected point.
     */
    template <typename T>
    static Vector2<T> mercator(const geom::Coordinates &mercator,
                               const T padding)
    {
        const FloatRect *mercator_space = DataBuffer::get_instance()->mercator_space;

        T x_min, x_max, y_min, y_max;
        if (typeid(T) == typeid(int))
        {
            const IntRect *texture_space = DataBuffer::get_instance()->texture_space;
            x_min = texture_space->left;
            x_max = texture_space->left + texture_space->width;

            y_min = texture_space->top;
            y_max = texture_space->top + texture_space->height;
        }
        else if (typeid(T) == typeid(float))
        {
            x_min = y_min = -1.f;
            x_max = y_max = 1.f;
        }

        Vector2<T> output;
        output.x = Functions::map_number(mercator.x,
                                         mercator_space->left, mercator_space->left + mercator_space->width,
                                         x_min + padding, x_max - padding);

        output.y = Functions::map_number(mercator.y,
                                         mercator_space->top, mercator_space->top + mercator_space->height,
                                         y_min + padding, y_max - padding);

        // const float bottom = mercator_space->top + mercator_space->height;
        // const float right = mercator_space->left + mercator_space->width;

        // cout << "MERCATOR [" << type << "]: "
        //      << endl
        //      << "  -> space : " << mercator_space->top << " " << mercator_space->left << " - " << bottom << " " << right << endl
        //      << "  -> output: " << output.x << " " << output.y << endl;

        return output;
    }

    // template <typename T>
    // static T mercator(const long value, const T padding, const long axis = 0)
    // {
    //     const FloatRect *mercator_space = DataBuffer::get_instance()->mercator_space;

    //     Rect<T> output_space;
    //     if (typeid(T) == typeid(int))
    //     {
    //         output_space = Rect<T>(*DataBuffer::get_instance()->texture_space);
    //     }
    //     else if (typeid(T) == typeid(float))
    //     {
    //         output_space = Rect<T>(*DataBuffer::get_instance()->unit_space);
    //     }

    //     T output;
    //     if (axis)
    //     {
    //         output = Functions::map_number(value,
    //                                        mercator_space->top, mercator_space->top + mercator_space->height,
    //                                        output_space.top + padding,
    //                                        output_space.top + output_space.height - padding);
    //     }
    //     else
    //     {
    //         output = Functions::map_number(value,
    //                                        mercator_space->left, mercator_space->left + mercator_space->width,
    //                                        output_space.left + padding,
    //                                        output_space.left + output_space.width - padding);
    //     }

    //     return output;
    // }
};