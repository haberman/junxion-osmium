#pragma once

// For cout
#include <iostream>

// For adjacent_find
#include <algorithm>

// For unique_nodes
#include <unordered_set>

// For junxions
#include <map>

// OSM types mapping.
#include <osmium/osm/location.hpp>
#include <osmium/geom/coordinates.hpp>

#include <SFML/Graphics.hpp>

using namespace std;
using namespace sf;
using namespace osmium;

/** @addtogroup io
 *  @{
 */

/**
 * Holds an OSM node as a transactional piece of mulitple projections.
 */
struct Point
{
public:
  /**
   * Empty constructor.
   * Here only to initialize `mercator` value.
   */
  // Point() noexcept: mercator({}) {}

  /** The `OSM` node id. */
  uint64_t uid;

  /** Lat/Lng location. */
  Location location;

  /** Mercator projection. */
  geom::Coordinates mercator;

  /** -1/1 projection. */
  Vector2f unit;

  /** pixel projection. */
  Vector2i texture;
};

/**
 * Holds two lists containing nodes being either before or after a junxion.
 * @fixme the way we identify an adjacent using a `way_id` is wrong since we could have multiple ajdacents on a single way.
 */
struct Adjacent
{
public:
  uint64_t way_uid;
  vector<Point> after;
  vector<Point> before;
};

/**
 * Describes a junxion, which is sort of an augmented regular OSM `node`.
 */
class Junxion
{
public:
  /**
   * Empty constructor
   */
  Junxion() {}

  /**
   * Constructs a new `Junxion` with an initial node id.
   * @param uid OSM node id
   */
  Junxion(const uint64_t uid);

  /**
   * Constructs a new `Junxion` with a complete OSM node.
   * @param uid OSM node id
   */
  Junxion(const uint64_t uid, const Location location);

  /**
   * Logs important informations about the `Junxion` content and state.
   */
  void print();

  unsigned connections = 1u;
  float weight = 0.f;

  Point point;
  map<uint64_t, Adjacent> adjacents;

  Junxion &operator=(const Junxion &);
};

/**
 * Singleton class that keeps pointers to chunks of memory used a little bit everywhere.
 */
class DataBuffer
{
public:
  /** Size of a pixel expressed as a fraction of (-1/1) screen space. */
  Vector2f *pixel;

  /** Size of a radius (in pixels) used to scale some 2D shapes. */
  float radius;
  
  /** Returns the vector of `pixel * radius`. */
  const Vector2f pixel_radius() const;

  //@{
  /** targets parser. */
  map<uint64_t, Junxion> junxions;
  unordered_set<uint64_t> junxions_unique_uids;
  //@}

  //@{
  /** targets drawer.
   * @fixme this is dumb to keep an SFML dependency when only jxn_parser is involved
  */
  const IntRect *texture_space;
  const FloatRect *mercator_space;
  const FloatRect *unit_space;

  const Font *monoid_font;
  //@}

  /** Returns the singleton. */
  static DataBuffer *get_instance();

  /** Frees the `intance_` pointer. */
  void destroy_instance();

private:
  /** The singleton instance. */
  static DataBuffer *instance_;

  /** Private constructor. */
  DataBuffer() = default;

  /** Private destructor. */
  virtual ~DataBuffer();
};

/**@}*/