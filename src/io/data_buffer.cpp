#include "data_buffer.hpp"

Junxion::Junxion(const uint64_t uid)
{
    point.uid = uid;
    point.location = Location();
}

Junxion::Junxion::Junxion(const uint64_t uid, const osmium::Location location)
{
    point.uid = uid;
    point.location = location;
}

Junxion &Junxion::operator=(const Junxion &junxion)
{
    point.uid = junxion.point.uid;
    point.location = junxion.point.location;
    connections = junxion.connections;

    return *this;
}

void Junxion::print()
{
    cout << "Junxion " << point.uid << endl;
    cout << "  -> location:   [" << point.location.lon() << ", " << point.location.lat() << "]" << endl;
    cout << "  -> mercator:   [" << point.mercator.x << ", " << point.mercator.y << "]" << endl;
    cout << "  -> unit: [" << point.unit.x << ", " << point.unit.y << "]" << endl;
    cout << "  -> connections:      " << connections << endl;
    cout << "  -> adjacents amount: " << adjacents.size() << endl;
}

DataBuffer *DataBuffer::instance_ = nullptr;

DataBuffer::~DataBuffer()
{
    delete mercator_space;
    delete texture_space;
    delete unit_space;
    delete pixel;
    delete monoid_font;
}

DataBuffer *DataBuffer::get_instance()
{
    if (!instance_)
    {
        instance_ = new DataBuffer;
        instance_->unit_space = new FloatRect({-1.f, -1.f}, {2.f, 2.f});
    }

    return instance_;
}

const Vector2f DataBuffer::pixel_radius() const
{
    const Vector2f v(pixel->x * radius, pixel->y * radius);
    return v;
}

void DataBuffer::destroy_instance()
{
    if (instance_)
    {
        delete instance_;
    }
}
