#include <cstdlib>  // for exit
#include <iostream> // for cout
#include <fstream>  // for ofstream

// JsonCPP
#include <json/json.h>

// Access PBF input files format
#include <osmium/io/pbf_input.hpp>

// Mercator projection
#include <osmium/geom/mercator_projection.hpp>

// To apply() handler
#include <osmium/visitor.hpp>

// Osmium data structures.
#include <osmium/osm/way.hpp>

// For the handler interface
#include <osmium/handler.hpp>
#include <osmium/handler/node_locations_for_ways.hpp>
#include <osmium/index/map/sparse_mem_array.hpp>

// Get access to isatty utility function and progress bar utility class.
#include <osmium/util/file.hpp>
#include <osmium/util/progress_bar.hpp>

/**
 * Class that facilitates arguments parsing.
 */
#include "io/input_parser.hpp"
#include "io/data_buffer.hpp"

using namespace sf;

/** @addtogroup parser
 *  @{
 */
struct JunxionHandler : public osmium::handler::Handler
{
    /**
     * Callback handler `struct` that's responsible for collecting junxions on top of `osmium`.
     * @param offset the amount of ajdacent nodes to join on each side of the way a junxion is on.
     */
    JunxionHandler(unsigned short offset) : adjacent_offset(offset) {}

    DataBuffer *data_buffer = DataBuffer::get_instance();

    const unsigned short adjacent_offset;

    uint64_t nodes = 0;
    uint64_t ways = 0;

    /**
     * @brief Retrieve locations of all nodes if the provided `way` is tagged as an `highway`.
     * Then compare thoses nodes to the internal memory where individual junxions ars stacked.
     * @param way The way to investigate
     */
    void way(const Way &way) noexcept
    {
        const char *highway = way.tags()["highway"];
        const char *cycleway = way.tags()["cycleway"];

        if (!highway && !cycleway)
        {
            return;
        }

        unsigned short idx = 0;

        for (const NodeRef &node_ref : way.nodes())
        {
            bool append_adjacent = false;
            const uint64_t node_uid = node_ref.positive_ref();
            const pair<unordered_set<uint64_t>::iterator, bool> unique_node = data_buffer->junxions_unique_uids.insert(node_uid);

            if (unique_node.second == false)
            {
                data_buffer->junxions[node_uid].connections++;
                append_adjacent = true;
            }
            else
            {
                data_buffer->junxions[node_uid] = Junxion(node_uid, node_ref.location());
                append_adjacent = true;
            }

            if (append_adjacent && adjacent_offset > 0)
            {
                add_adjacent(node_uid, way, idx, adjacent_offset);
            }

            ++idx;
        }
    }

    const Point adjacent_point(const NodeRef &node)
    {
        Point point;
        point.uid = node.positive_ref();
        point.location = node.location();

        return point;
    }

    void add_adjacent(uint64_t uid, const Way &way, unsigned short idx, const unsigned short offset)
    {
        Junxion &junxion = DataBuffer::get_instance()->junxions[uid];

        const NodeRefList &way_nodes = way.nodes();
        const unsigned short upper_limit = min((int)way_nodes.size(), idx + offset + 1);
        const unsigned short lower_limit = max(0, idx - offset - 1);

        Adjacent adjacent;
        adjacent.way_uid = way.positive_id();

        adjacent.after.empty();

        for (short l = idx; l >= lower_limit; --l)
        {
            adjacent.before.push_back(adjacent_point(way_nodes[l]));
        }
        for (unsigned short u = idx; u < upper_limit; ++u)
        {
            adjacent.after.push_back(adjacent_point(way_nodes[u]));
        }

        junxion.adjacents[adjacent.way_uid] = adjacent;
    }
};

string basename(const string &pathname, const bool remove_ext = true)
{
    string filename = pathname;

    const size_t last_slash_idx = pathname.find_last_of("\\/");

    if (string::npos != last_slash_idx)
    {
        filename.erase(0, last_slash_idx + 1);
    }

    if (remove_ext)
    {
        const size_t period_idx = filename.rfind('.');
        if (string::npos != period_idx)
        {
            filename.erase(period_idx);
        }
    }

    return filename;
}

Json::Value adjacent_line(vector<Point> points)
{
    Json::Value line = Json::arrayValue;

    for (const auto &p : points)
    {
        Json::Value adjacent = Json::arrayValue;
        adjacent.append(p.location.lon());
        adjacent.append(p.location.lat());

        line.append(adjacent);
    }

    return line;
}

Json::Value adjacent_geometry(const uint64_t uid, const string &side, vector<Point> points)
{
    Json::Value geometry;
    geometry["type"] = "LineString";
    geometry["coordinates"] = Json::arrayValue;

    for (const auto &p : points)
    {
        Json::Value adjacent = Json::arrayValue;
        adjacent.append(p.location.lon());
        adjacent.append(p.location.lat());

        geometry["coordinates"].append(adjacent);
    }

    // geometry["coordinates"].append(adjacent_line(points));
    geometry["properties"]["uid"] = Json::UInt64(uid);
    geometry["properties"]["side"] = side;

    return geometry;
}

int main(int argc, char **argv)
{
    InputParser input(argc, argv);

    if (input.argument_exists("-h"))
    {
        cout << "junxion-parser 0.4" << endl
             << endl;
        cout << "usage: jxn_parser [options] input.pbf" << endl;
        cout << "options: " << endl;
        cout << "  -h\tshow this help message and exit;" << endl;
        cout << "  -o\tindicates a file path to save the geojson file of intersections [default: './junxions.geojson'];" << endl;
        cout << "  -w\tminimum amount of ways for a node to be considered as a junxion [default: 3];" << endl;
        cout << "  -a\tnumber of adjacent nodes a junxion embed along each one of its underlying ways [default: 0 (disable)];" << endl;
        cout << "  -v\tincrease verbosity [default: false]." << endl;

        return EXIT_SUCCESS;
    }

    const string pbf_input = input.get_argument();

    if (pbf_input.find(".pbf") == string::npos)
    {
        cout << "[ERROR] Missing required pbf file input." << endl;

        return EXIT_FAILURE;
    }

    const string name = basename(pbf_input);
    const bool verbose = input.get_argument("-v", false);
    const unsigned short jxn_ways = input.get_argument("-w", 3);
    const unsigned short jxn_adjacents = input.get_argument("-a", 0);
    const string jxn_output = input.get_argument_s("-o", "junxions.geojson");

    auto otypes = osm_entity_bits::node | osm_entity_bits::way;

    io::File input_file{pbf_input};
    io::Reader reader{input_file, otypes};

    namespace map = index::map;
    using index_type = map::SparseMemArray<unsigned_object_id_type, Location>;
    using location_handler_type = handler::NodeLocationsForWays<index_type>;

    index_type index;
    location_handler_type location_handler{index};

    // Create an instance of our own CountHandler and push the data from the input file through it.
    JunxionHandler handler(jxn_adjacents);
    apply(reader, location_handler, handler);

    Json::Value geojson;
    geojson["type"] = "FeatureCollection";
    geojson["features"] = Json::arrayValue;

    for (auto const &j : DataBuffer::get_instance()->junxions)
    {
        Junxion junxion = j.second;

        if (junxion.connections >= jxn_ways)
        {
            const Location location = junxion.point.location;

            Json::Value feature;
            feature["type"] = "Feature";
            feature["properties"]["uid"] = Json::UInt64(j.first);
            feature["properties"]["connections"] = junxion.connections;
            feature["geometry"]["type"] = "GeometryCollection";
            feature["geometry"]["geometries"] = Json::arrayValue;

            Json::Value point;
            point["type"] = "Point";
            point["coordinates"] = Json::arrayValue;
            point["coordinates"].append(location.lon());
            point["coordinates"].append(location.lat());

            feature["geometry"]["geometries"].append(point);

            if (!junxion.adjacents.empty())
            {
                for (auto const &a : junxion.adjacents)
                {
                    if (a.second.before.size() > 1)
                    {
                        feature["geometry"]["geometries"].append(adjacent_geometry(a.second.way_uid, "before", a.second.before));
                    }

                    if (a.second.after.size() > 1)
                    {
                        feature["geometry"]["geometries"].append(adjacent_geometry(a.second.way_uid, "after", a.second.after));
                    }
                }
            }

            geojson["features"].append(feature);
        }
    }

    Json::StreamWriterBuilder json_builder;
    string document = Json::writeString(json_builder, geojson);

    ofstream outfile(jxn_output, ofstream::binary);
    outfile << document;
    outfile.close();

    if (verbose)
    {
        cout << "Parsed " << geojson["features"].size() << " junxions connecting at least " << jxn_ways << " different ways." << endl;
        cout << "Features exported to geojson file: " << jxn_output << endl;
    }

    reader.close();
    return EXIT_SUCCESS;
}

/**@}*/