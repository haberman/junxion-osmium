add_executable(${EXECUTABLE_NAME} src/${EXECUTABLE_NAME}.cpp
                                  src/io/data_buffer.cpp
                                  src/io/input_parser.cpp)
                                  
# Detect and add Osmium
find_package(Osmium REQUIRED COMPONENTS pbf sparsehash)
if(OSMIUM_FOUND)
   include_directories(SYSTEM ${OSMIUM_INCLUDE_DIRS})
   target_link_libraries(${EXECUTABLE_NAME} ${OSMIUM_PBF_LIBRARIES})
endif()

# Detect and add Jsoncpp
find_package(Jsoncpp REQUIRED)
if(JSONCPP_FOUND)
    include_directories(${JSONCPP_INCLUDE_DIRS})
    target_link_libraries(${EXECUTABLE_NAME} ${JSONCPP_LIBRARIES})
endif()

# Detect and add SFML (stupid...)
find_package(SFML 2 REQUIRED system window graphics) #network audio
if(SFML_FOUND)
    include_directories(${SFML_INCLUDE_DIR})
    target_link_libraries(${EXECUTABLE_NAME} ${SFML_LIBRARIES})
endif()
