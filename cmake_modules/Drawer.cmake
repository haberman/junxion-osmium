add_executable(${EXECUTABLE_NAME} src/${CMAKE_PROJECT_NAME}.cpp
                                  src/cgal/triangulator.cpp
                                  src/cgal/voronoi.cpp
                                  src/draw/gl_drawer.cpp
                                  src/draw/color_gradient.cpp
                                  src/io/data_buffer.cpp
                                  src/io/input_parser.cpp)
                                  

# Detect and add Jsoncpp
find_package(Jsoncpp REQUIRED)
if(JSONCPP_FOUND)
    include_directories(${JSONCPP_INCLUDE_DIRS})
    target_link_libraries(${EXECUTABLE_NAME} ${JSONCPP_LIBRARIES})
endif()

# Detect and add SFML
find_package(SFML 2 REQUIRED system window graphics) #network audio
if(SFML_FOUND)
    include_directories(${SFML_INCLUDE_DIR})
    target_link_libraries(${EXECUTABLE_NAME} ${SFML_LIBRARIES})
endif()

# Detect and add GMP
find_package(GMP REQUIRED)
if(GMP_FOUND)
    include_directories(${GMP_INCLUDE_DIR})
    target_link_libraries(${EXECUTABLE_NAME} ${GMP_LIBRARIES})
endif()

# Detect and add CGAL
find_package(CGAL REQUIRED)
if(CGAL_FOUND)
    include(${CGAL_USE_FILE})
    target_link_libraries(${EXECUTABLE_NAME} ${CGAL_LIBRARIES} ${CGAL_3RD_PARTY_LIBRARIES} )
endif()

# Detect and add OpenGL
set(OpenGL_GL_PREFERENCE "GLVND")
find_package(OpenGL REQUIRED)
if (OPENGL_FOUND)
    include_directories(${OPENGL_INCLUDE_DIR})
    target_link_libraries(${EXECUTABLE_NAME} ${OPENGL_LIBRARIES})
endif()

# Detect and add Osmium
find_package(Osmium REQUIRED COMPONENTS pbf sparsehash)
if(OSMIUM_FOUND)
   include_directories(SYSTEM ${OSMIUM_INCLUDE_DIRS})
   target_link_libraries(${EXECUTABLE_NAME} ${OSMIUM_PBF_LIBRARIES})
endif()
