# JUNXION - OSMIUM
This is a work in progress that's willing to become a geographic toolkit. It was developped in the frame of my artistic residency in the Interdisciplinary Center for Digital and Soud Culture [Transcultures](transcultures.be/2017/03/30/franck-soudan-laureat-pepinieres-europeennes-pour-jeunes-artistes-transcultures-charleroi-2017/). It currently toys around [osmium](http://osmcode.org/libosmium/) and [CGAL](https://www.cgal.org/) for input and computations and relays the output display to [SFML](https://www.sfml-dev.org/index.php). It comes with a [side project](https://framagit.org/haberman/junxion-automator) that automates the use of the 2 CLI programs:

## junXion parser 0.4
#### `jxn_parser [options] input.pbf`
```
options:
  -h    show this help message and exit;
  -o    indicates a file path to save the geojson file of intersections [default: './junxions.geojson'];
  -w    minimum amount of ways for a node to be considered as a junxion [default: 3];
  -a    number of adjacent nodes a junxion embeds along each one of its underlying ways [default: 0 (disable)];
  -v    increase verbosity [default: false].
```


#### dependencies :
- [libosmium](https://osmcode.org/libosmium/)
- [libjsoncpp](https://github.com/open-source-parsers/jsoncpp)

#### TODO's:
- optimizes intersections count so this...

    ``` 
            way 2
              |
              |
              |    
    way 1 ----+----
    ```

    ...counts as an intersection of 3 (and not 2). A way to do it would be to range intersections from w+1 to the actual amount of time we've met the same node id...

## junXion drawer 0.4
#### `jxn_drawer [options] junxions.geojson`
```
OPTIONS

* generic
  -h    show this help message and exit;
  -v    increase verbosity.
* output
  -w    width in pixels, used to create projection space [default: 2000];
  -m    mode, either: 'screen' or 'texture' [default: file];
  -d    directory to save images in 'screen' mode [default: ../output];
* drawing
  -M    map projection;
  -K    K(2)d spatial tree;
  -T    triangulation;
  -V    voronoi diagrams;
```


#### dependencies :
- [libosmium](https://osmcode.org/libosmium/)
- [libjsoncpp](https://github.com/open-source-parsers/jsoncpp)
- libsfml-dev
- libcgal-dev
- libgmp-dev

#### TODO's:
- make a better triangulation
- make a better quadtree
- make a better delaunay

## Licence
[WTFPL](http://www.wtfpl.net/) – Do What the Fuck You Want to Public License.
